#!/usr/bin/python3
#
# install requirement.txt
#
# in files having extra.EXTERNAL_CONTENT set in their front matter
# this script 
# - fetch external content
# - changes internal links
# - replace existing content

"""
this script is to be run when external documentation is updated
if fetches user documentation of different software directly on their gitlab repo
"""

import os
import toml
import requests

DISCLAIMER = """{% note(type="info", markdown=true) %}
This page was automatically added from [URL](URL). Please check here if something is broken.
{% end %}

"""

def replace_all_in(folder):
    """
    recursively check files inside given folder
    """
    for root, dirs, files in os.walk(folder):
        for name in files:
            filename = os.path.join(root, name)
            if filename.endswith(".md"):
                # only do this on markdown files
                replace(root, filename)

def replace(folder, filename):
    """
    if file has external content, fetch and use it
    - checks for EXTERNAL_CONTENT key in frontmatter
    - fetch external content
    - convert it to "zola style"
    - overwrites the file (do not commit content, this script is run by the CI)
    """
    # read file parts
    with open(filename) as f:
        text = f.read()
    front_matter_text = get_front_matter(text)
    front_matter = toml.loads(front_matter_text)
    # only continue if EXTERNAL_CONTENT is present
    if not ("extra" in front_matter.keys() and "EXTERNAL_CONTENT" in front_matter["extra"].keys()):
        return
    # fetch remote content
    print(filename)
    url = front_matter["extra"]["EXTERNAL_CONTENT"]
    print("requesting content")
    response = requests.get(url)
    print(response.status_code)
    # build new page
    page = make_page(folder, filename, url, front_matter_text, response.text)
    # overwrite file
    with open(filename, "w") as f:
        f.write(page)

def get_front_matter(text):
    """
    gets file front-matter between "+++" signs
    """
    parts = text.split("+++", maxsplit=2)
    return parts[1]

def modify(root, body):
    """
    transform text body to zola style
    """
    # instead of relative links, use absolute zola links
    # this allows for checking dead links
    body = body.replace("](./", f"](@/{root}/")
    # TODO if link to an image, use absolute link instead
    return body

def make_page(root, filename, url, front_matter_text, remote_content):
    """
    assemble page
    - front-matter
    - disclaimer
    - modified body 
    """
    # make disclaimer
    disclaimer = DISCLAIMER.replace("URL", url).replace("/raw/", "/blob/")
    # modify content to match zola's style markdown
    body = modify(root, remote_content)
    # return assembled page    
    return f"+++{front_matter_text}+++\n\n{disclaimer}\n{body}"

if __name__ == "__main__":
    os.chdir("content")
    replace_all_in("wiki/duniter-v2")