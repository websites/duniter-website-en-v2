::uml:: format="svg" alt="Diagramme réception, traitement puis retransmission d'un document via WS2P"

@startuml

[WS2P] -> serveur : recoit le bloc#2299
serveur -> [WS2P] : retransmet le bloc#2299

@enduml

::end-uml::