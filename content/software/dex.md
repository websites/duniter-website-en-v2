+++
title = "Dex"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-gears"
repo = ""
website = ""
+++

<a href="https://git.duniter.org/nodes/typescript/duniter/-/tree/dev/rust-bins/duniter-dbex">Dex</a> is a Duniter database explorer.
