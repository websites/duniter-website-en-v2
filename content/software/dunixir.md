+++
title = "Dunixir"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "/img/dunixir.png"
repo = "https://gitlab.imt-atlantique.fr/dunixir/dunixir"
website = ""
+++

<a href="https://gitlab.imt-atlantique.fr/dunixir/dunixir">Dunixir</a> is a school project by ITM Atlantic students to implement Duniter in Elixir.
