+++
title = "Ğsper"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "/img/gsper-logo.svg"
repo = ""
website = "https://g1.frama.io/gsper/"

+++

<a href="https://g1.frama.io/gsper/">Ğsper</a> allows you to try to recover a lost password by brute force.
