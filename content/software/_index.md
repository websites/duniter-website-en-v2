+++
title = "Software"
template = "software/all.html"
page_template = "software/single.html"
weight = 3
description = "Software ecosystem of Ğ1 currency"

[extra]
item_path = "software/"

[[extra.group]]
name = "Software ecosystem"
list = ["duniter", "cesium", "silkaj", "gchange", "wotwizard", "datapods", "ginkgo", "g1superbot"]
group_by = 3
[[extra.group]]
name = "Under development"
list = ["duniter-v2", "gcli-v2", "duniter-squid", "cesium-v2", "gecko", "tikka"]
group_by = 3
[[extra.group]]
name = "Libraries"
list = ["duniterpy", "g1lib", "durt"]
group_by = 3
[[extra.group]]
name = "Many utilities"
list = [
    "remuniter",
    "barre-integrable",
    "gsper",
    "dex",
    "gmixer",
    "gcli",
    "dup-tools",
    "vanitygen",
    "wotmap",
    "animwotmap",
    "worldwotmap",
    "g1cotis",
    "g1pourboire",
    "g1sms",
    "g1billet",
    "g1tag",
    "g1lien",
    "g1-monit",
    "gakpot",
    "little-tools",
    "jaklis",
    "bog",
    "g1-stats",
    "dunixir",
    "gexplore",
    "wotwizard-ui",
    "vignette",
    "kazou",
    "ginspecte",
    "g1-companion",
]
group_by = 4

+++
