+++
title = "Cesium v2"

[taxonomies]
authors = ["kimamila", "txels"]
language = ["typescript",]
framework = ["angularjs", "ionic"]

[extra]
logo = "/img/cesium-v2.svg"
repo = "https://git.duniter.org/clients/rust/gcli-v2s/"
website = "https://cesium.app/fr/"
+++

Based on <strong>AngularJS</strong> and <strong>Ionic</strong> frameworks, <a href="https://cesium.app/">Cesium v2</a> is a web client also available on smartphone.