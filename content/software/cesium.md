+++
title = "Cesium"

[taxonomies]
authors = ["kimamila", "matograine"]
language = ["javascript",]
framework = ["angularjs", "ionic"]

[extra]
logo = "/img/cesium.svg"
repo = "https://git.duniter.org/clients/cesium-grp/cesium/"
website = "https://cesium.app/fr/"
+++

Based on <strong>AngularJS</strong> and <strong>Ionic</strong> frameworks, <a href="https://cesium.app/">Cesium</a> is a web client also available on smartphone.
