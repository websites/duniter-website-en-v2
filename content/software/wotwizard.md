+++
title = "WotWizard"

[taxonomies]
authors = ["gerard94", ]
language = ["go", ]
framework = []

[extra]
logo = "/img/wizard.svg"
repo = "https://git.duniter.org/gerard94/wotwizard"
+++


Developed in <strong>Go</strong> and offering a <strong>GraphQL</strong> API, <a href="https://github.com/duniter/WotWizard/">WotWizard</a> provides information on the history of the web of trust as well as predictions on the entries.
