+++
title = "Ğ1 monit"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-bolt"
repo = "https://git.duniter.org/nodes/typescript/modules/duniter-currency-monit"
website = ""
+++

<a href="https://git.duniter.org/nodes/typescript/modules/duniter-currency-monit">ğ1-monit</a> is a duniter module for generating various statistics on the libre currency Ğ1.
