+++
title = "Ğ1 stats"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-line-chart"
repo = ""
website = "https://g1-stats.axiom-team.fr/"
+++

<a href="https://g1-stats.axiom-team.fr/">Ǧ1 stats</a> is a <strong>bash</strong> utility for analysing transactions.
