+++
title = "Ğ1Cotis"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-cube"
repo = ""
website = "https://g1pourboire.fr/G1cotis.html"
+++


<a href="https://g1pourboire.fr/G1cotis.html">Ğ1Cotis</a> allows a percentage of transactions to be automatically transferred to a target account.
