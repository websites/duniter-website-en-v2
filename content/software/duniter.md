+++
title = "Duniter"

[taxonomies]
authors = ["cgeek", "elois"]
language = ["javascript", "rust"]
framework = ["nodejs"]

[extra]
logo = "/img/duniter-logo_alt.svg"
repo = "https://git.duniter.org/nodes/typescript/duniter/"
website = "https://duniter.org/"
+++

The Ğ1 blockchain is powered by <a href="https://git.duniter.org/nodes/typescript/duniter/">Duniter</a>. Before in <strong>Node.js</strong>, a progressive migration to <strong>Rust</strong> has been engaged.