+++
title = "Ğ1Pourboire"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-money"
repo = ""
website = "https://g1pourboire.fr/"

+++

<a href="https://g1pourboire.fr/">Ğ1Pourboire</a> allows you to print access codes to a dedicated wallet that can be used to leave a tip.
