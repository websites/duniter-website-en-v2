+++
title = "ViĞnette"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "/img/vignette.svg"
repo = ""
website = "https://g1cotis.fr/vignette/"

+++

<a href="https://g1cotis.fr/vignette/">ViĞnette</a> is a QR code generator that makes it easy to share your public key.
