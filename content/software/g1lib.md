+++
title = "Ğ1lib"

[taxonomies]
authors = ["1000i100"]
language = ["javascript", "typescript"]
framework = []

[extra]
logo = "/img/g1js.svg"
repo = "https://git.duniter.org/libs/g1lib.js/"
website = ""
+++

<strong>JavaScript</strong> library allowing to handle cryptographic keys, <a href="https://git.duniter.org/libs/g1lib.js/">ğ1lib</a> is used by Ğsper and Ğ1-companion.
