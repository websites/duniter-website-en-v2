+++
title = "VanityGen"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-book"
repo = "https://github.com/jytou/vanitygen"
website = ""

+++

<a href="https://github.com/jytou/vanitygen">VanityGen</a> allows you to create a public key containing a certain scheme.
