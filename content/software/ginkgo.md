+++
title = "Ğ1nkgo"

[taxonomies]
authors = ["vjrj", ]
language = ["dart", ]
framework = ["flutter", ]

[extra]
logo = "/img/ginkgo.svg"
repo = "https://git.duniter.org/vjrj/ginkgo/"
website = "https://g1nkgo.comunes.org/"
+++

<strong>Flutter</strong> app based on Duniter 1.9 GVA API allowing to perform fast payments.
