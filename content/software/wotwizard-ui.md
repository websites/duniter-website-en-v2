+++
title = "WotWizard UI"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "/img/wotwizard-ui.gif"
repo = ""
website = ""

+++


<a href="https://git.duniter.org/clients/wotwizard-ui">WotWizard UI</a> is an ergonomic UI for WotWizard (<a href="https://forum.duniter.org/t/wotwizard-ui/8939">forum</a>).
