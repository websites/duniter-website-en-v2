+++
title = "WotMap"

[taxonomies]
authors = ["paidge", ]
language = []
framework = []

[extra]
logo = "/img/wotmap.jpg"
repo = ""
website = "https://wotmap.duniter.org/"

+++

La <a href="https://wotmap.duniter.org/">WotMap</a> is a software for visualizing the web of trust as a graph.

