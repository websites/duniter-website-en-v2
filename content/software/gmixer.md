+++
title = "Ğmixer"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "/img/gmixer.svg"
repo = ""
website = "https://zettascript.org/projects/gmixer/"

+++

<a href="https://zettascript.org/projects/gmixer/">Ğmixer</a> is an Ğ1 wallet anonymizer (not to be confused with <a href="https://forum.monnaie-libre.fr/t/gmix-anonymiseur-de-porte-feuille-g1/862">ĞMix</a>).
