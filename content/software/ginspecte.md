+++
title = "Ğinspecte"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "/img/ginspecte.svg"
repo = ""
website = "https://g1-status.mithril.re/"

+++

<a href="https://g1-status.mithril.re/">Ğinspecte</a> is a panel for monitoring instances of different software.
