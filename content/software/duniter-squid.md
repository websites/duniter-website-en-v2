+++
title = "Duniter-squid"

[taxonomies]
authors = []
language = ["typescript"]
framework = []

[extra]
logo = "/img/duniter-squid.svg"
repo = "https://git.duniter.org/nodes/duniter-squid"
website = ""

+++

<a href="https://git.duniter.org/nodes/duniter-squid">Duniter-squid</a> is an indexer written in <strong>Typescript</strong> within the `squid` framework.
