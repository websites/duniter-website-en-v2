+++
title = "Duniter-v2s"

[taxonomies]
authors = ["elois", "HugoTrentesaux", "tuxmain", "bgallois"]
language = ["rust"]
framework = ["substrate"]

[extra]
logo = "/img/duniterv2.svg"
repo = "https://git.duniter.org/nodes/rust/duniter-v2s"
website = "https://duniter.fr/"
+++


<a href="https://git.duniter.org/nodes/rust/duniter-v2s">Duniter-v2s</a> is the version 2 of Duniter completely re-written on Substrate blockchain framework.
