+++
title = "Ğcli-v2"

[taxonomies]
authors = []
language = ["rust"]
framework = ["subxt"]

[extra]
logo = "/img/gcli.svg"
repo = "https://git.duniter.org/clients/rust/gcli-v2s/"
website = ""

+++

<a href="https://git.duniter.org/clients/rust/gcli-v2s/">Ğcli-v2</a> is a command line client written in <strong>Rust</strong> with `subxt`.
