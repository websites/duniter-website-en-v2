+++
title = "Ğecko"

[taxonomies]
authors = ["poka", ]
language = ["dart", ]
framework = ["flutter", ]

[extra]
logo = "/img/gecko.png"
repo = "https://git.p2p.legal/axiom-team/gecko"
website = ""
+++

The <strong>Flutter</strong> framework allows the transaction-oriented <a href="https://git.p2p.legal/axiom-team/gecko">Ğecko</a> mobile app to reach top performances. Wallets are managed using <strong>Rust</strong> bindings and data travel through GVA and Datapods.

