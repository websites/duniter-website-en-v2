+++
title = "Animwotmap"

[taxonomies]
authors = ["HugoTrentesaux", ]
language = ["julia", ]
framework = []

[extra]
logo = "fa-play-circle"
repo = ""
website = ""
+++

<a href="https://forum.monnaie-libre.fr/t/la-toile-de-confiance-animee/11132">Animwotmap</a> generates an animated visualisation of the history of the web of trust.
