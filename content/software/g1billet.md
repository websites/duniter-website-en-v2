+++
title = "Ğ1Billet"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-money"
repo = ""
website = "https://g1sms.fr/fr/g1billet"
+++

<a href="https://g1sms.fr/fr/g1billet">Ğ1Billet</a> allows you to print your own money, with <strong>QR codes</strong> and scratch-off notes.
