+++
title = "Ğcli"

[taxonomies]
authors = []
language = ["rust"]
framework = []

[extra]
logo = "fa-terminal"
repo = ""
website = ""

+++

<a href="https://git.duniter.org/clients/rust/gcli">Ğcli</a> is a command line GVA client written in <strong>Rust</strong>.
