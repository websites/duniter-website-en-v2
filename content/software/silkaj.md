+++
title = "Silkaj"

[taxonomies]
authors = ["moul", "vit"]
language = ["python"]
framework = []

[extra]
logo = "/img/silkaj.svg"
repo = "https://git.duniter.org/clients/python/silkaj/"
website = "https://silkaj.duniter.org/"
+++

Command line client developed using <strong>Python</strong>, <a href="https://silkaj.duniter.org/">Silkaj</a> allows to automate complex tasks.
