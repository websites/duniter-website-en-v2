+++
title = "Jaklis"

[taxonomies]
authors = []
language = ["python", ]
framework = []

[extra]
logo = "/img/jaklis.png"
repo = "https://git.p2p.legal/axiom-team/jaklis"
website = ""
+++


<a href="https://git.p2p.legal/axiom-team/jaklis">Jaklis</a> is a command line client written in <strong>python</strong> for Cesium+ and Ğchange datapods.
