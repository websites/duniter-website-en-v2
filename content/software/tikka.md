+++
title = "Tikka"

[taxonomies]
authors = ["vit", ]
language = ["python", ]
framework = ["pyqt", ]

[extra]
logo = "/img/tikka.png"
repo = ""
website = ""
+++

<a href="https://forum.duniter.org/t/naissance-aujourdhui-de-tikka-un-nouveau-client/7849">Tikka</a> is a business-oriented desktop client under development.
