+++
title = "Kazou"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-music"
repo = ""
website = ""

+++

<a href="https://g1cotis.fr/kz/">Kazou</a> is a tool to observe the Duniter network and find a node in good condition.
