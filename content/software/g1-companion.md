+++
title = "Ğ1-companion"

[taxonomies]
authors = ["manutopik"]
language = ["javascript"]
framework = ["nuxt"]

[extra]
logo = "/img/g1compagnon.svg"
repo = "https://git.duniter.org/clients/g1-compagnon"
+++


Browser extension to manage keys. It expose and API allowing any website to provide Ǧ1 related features.
