+++
title = "Datapods"

[taxonomies]
authors = ["kimamila"]
language = ["java"]
framework = ["elasticsearch"]

[extra]
logo = "/img/datapod.svg"
repo = "https://git.duniter.org/clients/cesium-grp/cesium-plus-pod/"
website = "http://doc.e-is.pro/cesium-plus-pod/REST_API.html"
+++

The <a href="http://doc.e-is.pro/cesium-plus-pod/REST_API.html">Datapods</a> are an off-blockchain data layer based on <strong>ElasticSearch</strong> which is used by client apps like Cesium and Ğchange.
