+++
title = "Funding bar"

[taxonomies]
authors = ["paidge", ]
language = []
framework = []

[extra]
logo = "fa-battery-half"
repo = ""
website = ""
+++

The <a href="https://git.duniter.org/paidge/barre-de-financement-int-grable">funding bar</a> allows a progress bar to be integrated into a website via a <span style="font-family: monospace;">&lt;iframe/&gt;</span> in order to monitor the progress of crowdfunding.
