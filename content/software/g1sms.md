+++
title = "ĞSMS"

[taxonomies]
authors = []
language = []
framework = []

[extra]
logo = "fa-comment"
repo = ""
website = "https://www.g1sms.fr/"

+++

<a href="https://www.g1sms.fr/">ĞSMS</a> is an SMS payment system that facilitates access to money for the less tech-savvy.
