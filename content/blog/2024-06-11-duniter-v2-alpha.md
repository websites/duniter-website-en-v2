+++
title = "Duniter v2 alpha 🌀"
description = "It has been a while since we started working on Duniter v2 implementation in Substrate framework. We announce here a new milestone."

[extra]
thumbnail = "/img/duniterv2-alpha.png"

[extra.translations]
fr = "blog/duniter-v2-alpha/"

[taxonomies]
authors = ["HugoTrentesaux",]
# tags = []
+++

<br>
<br>

{% note(type="info") %}
This article contains some of the author's opinions which might not be shared by other Duniter contributors. (marked by '🞰')
{% end %}

# Duniter v2 alpha 🌀

It's been a while since we announced Duniter version 2 in [this blog post](@/blog/2022-04-21-duniter-substrate.md). Since that time, a lot of work has been achieved, not only on [Duniter v2 itself](@/software/duniter-v2.md), but on the whole v2 ecosystem including the [Duniter Squid indexer](@/software/duniter-squid.md) and the version 2 clients like [Cesium v2](@/software/cesium-v2.md) and [Ğecko](@/software/gecko.md). It is now time to announce the Duniter v2 alpha, ready for testing with the `gdev` network.

[TOC]

## Still a solo chain 💫

Duniter v1 is a solo chain run by members of the Ğ1 web of trust. While using the Polkadot <abbr title="Software Developement Kit">SDK</abbr> (Substrate), we do not want to be a parachain of {{si(id="polkadot")}}Polkadot or Kusama for technical and political reasons. On the technical side, the web of trust requires to store a lot of onchain data and we do not want the `proof_size` limitation of parachains which limits the size of data coming from the merkle tree used during the blocks execution. On the political side, we want the tools to be in the hands of their users forever and cannot delegate the consensus to a relay chain that costs "non-libre money", even if fees are offered within the [common good parachain](https://polkadot.network/blog/proposal-for-common-good-parachains/) program. That's why we invite Ğ1 members who care about independence to join the `gdev` test network.

## Join the test network ☄

We have run multiple test networks in the past to get familiar with the technology. Now that the software is stabilizing, we are ready to welcome more people on our `gdev` test network. You can join as a mirror node if you simply want to discover, and announce a public RPC endpoint if you want to increase decentralization. If you feel confident and want to take part in the consensus, you can even run a smith node. The [documentation is available in the wiki section](@/wiki/duniter-v2/_index.md).

The test network is a way to get familiar with Duniter identity system. Since it's a test network you can get certified without <abbr title="In Real Life">IRL</abbr> meetings simply by giving a public key in the [forum Ğdev section](https://forum.duniter.org/c/currencies/gdev/63). The clients in beta version are configured to connect to this test network by default.

## Blockchain as a common good 🌲

We already mentioned the "software as a common good" approach of Duniter project in the [previous post](@/blog/2022-04-21-duniter-substrate.md#blockchain-as-a-common-resource-evergreen-tree):

> Common good: *"a resource that community manages for individual and collective benefit"*

We worked further on this topic with the Ğ1 community, in particular to address the fees problem, but also on governance aspects. Details below.

### A solution to the fee problem 💸

Blockchain is a truly shared and open resource, not as a multiuser system that limits SSH connections to a set of known pre-configured keys, but as a virtual machine that runs the same code in a distributed manner. It has a permission system and must take execution cost seriously if it doesn't want to be attacked by denial of service. Since it's a distributed system, there is no way to simply rely on per-ip limitations and other measures have to be implemented.

Users are identified by a cryptographic public key with "infinite" number of possibilities in the sense that it's extremely improbable that the same key is picked twice with proper random generation (IPv4 is 32 bits, IPv6 is 128 bits, we use ed25519 public keys with 256 bits). This is necessary for key security but prevents applying a per-key quota. Because we have a web of trust, we could limit the use of the blockchain to a restricted set of keys, but that would prevent anonymous accounts (not linked to any part of the wot) from using the blockchain. All public blockchains we heard of came up with the same solution: fees. Any operation on the blockchain costs money to the author which limits the amount of resource he can afford. In addition, validators can prioritize transactions based on self-interest through a tipping mechanism.

This solution has multiple issues incompatible with our vision of software as a common good and an egalitarian currency:
- the use of the common resource is not *really* free, it comes with a cost
- those who own more have more rights to use the blockchain (they pay less fees in proportion of their total wealth)
- those who own more are privileged over the other (they can pay a higher tip to the validator to get prioritized)

In some situations like {{si(id="ethereum")}}Ethereum, the cost ("gas") became so prohibitive that many users left the platform. In some situations like {{si(id="solana")}}Solana, the base cost is so low that <a href="https://en.wikipedia.org/wiki/Denial-of-service_attack"><abbr title="Distributed Denial of service">DDos</abbr> attacks</a> aren't expensive enough and occur often, triggering prioritization fees.

In our research of finding the best of both worlds, we came up with a solution that should allow a totally free blockchain use most of the time and a free blockchain use within a quota for humans (identified by the web of trust) during the attacks. This solution is based on substrate's good benchmarking framework. Since we know quite precisely the worst case scenario of each action in terms of storage size and execution time, as well as the maximum capacity of the reference machine, we can set a threshold of activity below which every action is free of charge. When this threshold is exceeded — most likely caused by an attack — the fees stop being null and make the attack stop by lack of "fuel". During these attacks, the non-anonymous accounts (= linked to the web of trust) still enjoy null fees within a certain quota.

We think that this solution will encourage people tired of bank fees to try our blockchain and make some room for unexpected applications that would be financially irrelevant with per-transaction fees like transparent account-based cost accounting for organizations.

### Blockchain governance for runtime upgrades 🆙

Advanced onchain governance systems ("<a href="https://en.wikipedia.org/wiki/Decentralized_autonomous_organization"><abbr title="Decentralized Autonomous Organization">DAO</abbr></a>") for runtime upgrades like the one leveraged by Kusama are interesting, but:
- it hides the fact that software development is strongly financially driven
- it's not well-suited for the rather small-sized, strongly linked, non-technical Ğ1 community

For the moment, we simply want to make the way things work in v1 more transparent. First, developers come up with an idea, they voluntarily create an implementation, discuss it with the other developers, and submit it to the [smiths](@/wiki/duniter-v2/glossary.md). Smiths aren't necessarily tech people, but they trust the developers and *do* install the update. In v2 we needed to secure the smith group by a smith web of trust (more on that later) and we needed to get more responsive for the runtime upgrades, hence, a well-informed technical committee. We first thought it would be a centralization issue for the community but we found that: 1️⃣ this is a particularity of forkless consensus systems that a more sophisticated voting system can help scale but not fix (🞰), 2️⃣ this is not what the community expects when it comes to software development, hence the next section (🞰).

### Software ecosystem built with the community (🞰) 🤝

What the community *really* expects is not theoretical perfection in the decision-making process but the evolution of the final app they will have in their hands. In other words, the way UIs are crafted is more important to the average user than the core system they rely on. It's more important that their feature requests are implemented in a reasonable amount of time than the process of this implementation. So we will first focus our efforts on establishing and maintaining a high-quality link between the community's will and the implementation and only then implement self-governance mechanisms in a rather "bottom-up" than "top-down" manner. This means we will likely first experiment with offchain information and voting systems before implementing them onchain.

Stay tuned if you are interested in our developments and do not hesitate to fund the project to help it grow!