+++
title = "Crowdfunding of the Ğ1v2"
date = 2022-11-05
description = "Help G1 currency to grow!"

[extra]
thumbnail = "/blog/g-pot.svg"

[taxonomies]
authors = ["MissSophie",]

[extra.translations]
fr = "blog/financement-participatif/"
+++

# Crowdfunding of the Ğ1v2

Until now, Duniter's development has been happening **only on a voluntary basis**, although supported by a Ǧ1 donation fund.

<span class="center-content">
<a href="https://forum.duniter.org/t/caisse-de-dons-pour-les-contributeurs-techniques-a-lecosysteme-g1/6908" class="w3-round w3-button w3-blue">💰 Ğ1 developers donation box</a>
</span>

As you may have heard, we are preparing a version 2 of Duniter that will fix all known bugs such as:

- pool synchronization
- choice of the Duniter node

and will provide a much better user experience:

- a block calculation every 6 seconds instead of 5 minutes
- validation of certifications at the time of issue
- new and much faster client software (Cesium v2, Ğecko)

<span class="center-content">![g-pot](/blog/g-pot.svg)</span>
{% quote() %}
Help G1 currency to grow!
{% end %}


The **ǦDev**, which is the test currency for this new version of Duniter, was launched in May 2022 during the **RML16** (the 16th edition of the big G1 currency technical meeting) hosted by Philippe Guillemant. While the G1 currency was developed in France by French developers a few years ago, a lot of Spanish contributors joined the G1 developers community during this sixteenth technical meeting. This was a confirmation that we are ready to move to the next level.

<span class="center-content">📝 Report of the RML16 ([Content in French](https://forum.monnaie-libre.fr/t/compte-rendu-des-rml-16-a-destination-des-utilisateurs-de-la-monnaie-libre/23118)) / 
📹 Video of the RML16 ([partly in English](https://crowdbunker.com/v/Xsg4y3BF3J))</span>

To keep the community informed of changes coming up, @Elois gave a talk at the G1 currency summer school near Toulouse : "Ğ1 v2.0 : What will change".

<span class="center-content">🎤 Conference "Ğ1 v2.0: What will change" ([in French](https://forum.monnaie-libre.fr/t/conference-g1-v2-0-ce-qui-va-changer/23642)) / ▶️ on peertube ([in French](https://tube.p2p.legal/w/eVrbpbSQB2cuhnYk7Wrhyy)) </span>

We are going to share more on other media platforms to explain the new ecosystem as clearly as possible, in order to prepare for a smooth transition within the next two years.

---

This migration requires a significant amount of work beforehand. Volunteering on evenings and weekends will not be enough to get there, and we might get to a point where we would get exhausted. Fortunately, many of us are willing to put aside our € paid jobs to dedicate the precious daytime hours.

To ensure that this work is achieved in the best conditions, we must be able to meet the primary needs of those involved (rent, utilities, food) for at least a period of one year.

As these needs cannot be met exclusively in Ǧ1 currency for the time being, we have decided to crowdfund for donations in €. Members and users of the Ǧ1 currency who wish to support its technical development and can afford it are invited to participate in this crowdfunding campaign.

<span class="center-content">
<a href="https://www.helloasso.com/associations/axiom-team" class="w3-round w3-button w3-deep-orange">💶 Crowdfunding in €</a>
</span>

This first call for donations should give us the freedom to set up a better and more sustainable funding system. We will give more details about these solutions some time in the near future.

---

*Description of the expenses:*
The amount collected will be distributed in full transparency among the developers according to the distribution circle method (see Financial Co-responsibility explanations - [in French](https://forum.duniter.org/t/co-responsabilite-financiere/9720)).

*Beneficiaries of the crowdfunding:*
Only developers listed on [the contributors page](@/team/_index.md) can participate in the distribution circle.

*Project managers:*
Axiom Team has been chosen to be an intermediary with the "euro world". We have a HelloAsso page and a connected bank account, which allows us to make transfers according to the decisions of the distribution circle.  
See Axiom Team on the G1 currency forum: [https://forum.monnaie-libre.fr/t/a-propos-de-la-categorie-axiom-team/24375](https://forum.monnaie-libre.fr/t/a-propos-de-la-categorie-axiom-team/24375)

*Location of this project:* France

*Donations (French "dons"):*
- Give anything from 1€ to participate
- Give 10 euros to fund a meal  
- Give 20 euros to fund one month of internet bill
- Give 100 euros to fund 3 months of electricity bill
- Give 500 euros to fund 1 month of rent
