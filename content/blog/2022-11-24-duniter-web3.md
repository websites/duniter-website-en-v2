+++
title = "Duniter and web3"
date = 2022-11-24
description = "Web3 intends to democratize the use of decentralised architectures and provide the online world with a democratic governance model. Here is what Duniter has to bring to it."

[extra]
thumbnail = "/blog/web3.svg"

[extra.translations]
fr = "blog/duniter-web3/"

[taxonomies]
authors = ["HugoTrentesaux",]
tags = ["proof of personhood", "proof of identity", "web of trust"]

+++

<br>
<br>

{% note(type="warning") %}
This article is an opinion piece. Its content should be understood as the author's point of view.
{% end %}


# Duniter and Web3
[Web3](https://en.wikipedia.org/wiki/Web3) is the name given to an ideal future architecture of the web where blockchain technology allows decentralized governance and token-based economics (instead of the current mostly *ad-based*  model). But currently Web3 initiatives seem limited to a technology-enthusiast elite already well integrated with the online world and does not help closing the digital divide. It is not inclusive enough of the working class and tech fearful people to reach its full democratic potential.

**Duniter software** addresses this issue by implementing the <abbr title="Relative Theory of Money">RTM</abbr>, a sound monetary theory proving that the only egalitarian monetary creation is in the form of a **Universal Dividend (UD)** (not to be confused with an universal basic income, UBI). Its **Web of Trust (WoT)** serves as a basis for decentralized digital identity, allowing the allocation of this UD.

A life-size experiment named **Ğ1 currency** and started in 2017 and confirms that the UD is a valid way to tackle social inequalities and provide a true unit of measure of value. At this point, the Ğ1 WoT counts 6971 individuals with a high social diversity. It is making itself ready to scale up thanks to the [rewriting of Duniter](@/blog/2022-04-21-duniter-substrate.md) in the Substrate blockchain framework, allowing to benefit from its scalability and *on-chain* governance ecosystem.


<span class="center-content"><img src="/blog/web3.svg" alt="duniter in web3 logo" style="max-width: 200px;"/></span>

> Duniter and web3. Credits [@imppao](https://www.youtube.com/@imppaofree)

## Libre Money
Ğ1 is a *libre money* (as [*libre software*](https://www.gnu.org/philosophy/free-sw.en.html), meaning "free" as in "freedom") in the sense given by the RTM: a money in which monetary creation *does not* introduce inequalities. The RTM proves such a currency *must* be issued in the form of an *Universal Dividend* (UD), distributed periodically among the users in proportion of the total monetary mass. As a corollary, any currency not implementing a UD *does* introduce inequalities. For example Bitcoin favours the ones arrived early and the ones owning powerful machines, Ethereum benefits to the early joiners and since 2022 advantages the stakers (the one already owning a lot of tokens), fiat currencies advantage the [*wasp*](https://en.wikipedia.org/wiki/White_Anglo-Saxon_Protestants) and so on...

The growth rate of a *libre money* can be chosen at start but is fixed once and for all (at constant demography). Ğ1 is targeting people with a life expectancy of ~80 years and the choice of a 10% annual growth rate was made so that the total monetary mass comes in equal parts from living people and dead people ([ref](https://www.creationmonetaire.info/2019/08/c-fvx.html) in French) in the hypothesis of a constant demography.

## Web of Trust (WoT)
Despite cryptography ensuring the authenticity of a document thanks to [digital signature](https://en.wikipedia.org/wiki/Digital_signature), it does not establish the link between the digital identity and the individual. This problem known as [proof of personhood](https://en.wikipedia.org/wiki/Proof_of_personhood) (PoP) (or *proof of humanity*) is known to be difficult.
The [Web of Trust](https://en.wikipedia.org/wiki/Web_of_trust) approach used in PGP in an attempt to address it (*i.e.* establishing the binding between a pubkey and its owner) seems [dead](https://inversegravity.net/2019/web-of-trust-dead/) now. A workshop named *Rebooting the Web of Trust* ([weboftrust.info](https://www.weboftrust.info/)) has been working on rebooting this since 2015 through annual <abbr title="Rebooting the Web Of Trust">RWOT</abbr> events with no concrete implementation yet.

Duniter's Web of Trust, thanks to its clever mix of temporal and static rules ([read more](@/blog/2018-06-18-duniter-deep-dive-wot.md)), has allowed the Ğ1 community to grow in a steady and sustained manner. Its relative slow growth appeared beneficial to the "Trust" part of Web of *Trust*. The members are <a href="https://carte.monnaie-libre.fr/?members">well spread over the French territory</a> both in urban and rural areas including territories overseas. Ğ1 community is involved in Duniter governance through many off-chain <abbr title="In Real Life">IRL</abbr> meetings. 

<a href="https://youtu.be/Hj3GpaEYLwA" target="_blank" class="center-content"><img src="/blog/wot_2022-11-22.png" alt="web of trust" style="max-width: 400px;"/></a>

> snapshot of the web of trust on 2022-11-22: 6944 members, 61737 certifications. Animation available on [https://youtu.be/Hj3GpaEYLwA](https://youtu.be/Hj3GpaEYLwA)

## Proof of identity blockchain consensus
Blockchain consensus algorithms can be [permissionless or permissionned](https://en.wikipedia.org/wiki/Consensus_(computer_science)#Permissioned_versus_permissionless_consensus). Known permissionless algorithms like Bitcoin's *proof of work* are energy intensive and slow. This is why a lot of blockchains headed towards permissionned algorithms like *proof of stake* or *proof of authority*. While Duniter v1 used a mixed proof ([read more](@/blog/2018-11-27-duniter-proof-of-work.md)), Duniter v2 is now ready to use the fully decentralized *proof of identity* mechanism provided by its Web of Trust in combination with the hybrid
<a href="https://research.web3.foundation/Polkadot/protocols/block-production/Babe"><abbr title="Blind Assignment for Blockchain Extension">BABE</abbr></a>/<a href="https://research.web3.foundation/Polkadot/protocols/finality"><abbr title="Greedy Heaviest-Observed Sub-Tree">G</abbr><abbr title="GHOST-based Recursive ANcestor Deriving Prefix Agreement">RANDPA</abbr></a>
algorithm. While the main *human* WoT is available to anybody, a subset of it called *smith* WoT with high security requirements will grant access to the set of block-authoring members (a.k.a *authorities*). It will then be the first blockchain consensus using a fully decentralized [proof of identity](https://en.wikipedia.org/wiki/Proof_of_identity_(blockchain_consensus)) (PoID) permission layer for BABE. It is an other way to get closer to real on-chain democracy (the next existing closest way being [NPoS](https://wiki.polkadot.network/docs/learn-consensus#nominated-proof-of-stake) in my opinion).

## Tool-oriented community-driven design
While many Web3 projects apply product-oriented design, Duniter software ecosystem is tool-oriented. It does not try to create need with aggressive marketing, it caters to different user needs. Just looking the galaxy of [free software connected to Duniter's blockchain](@/software/_index.md) gives an idea of the diversity of use of this currency compared to the mainly financial aspect of crypto-tokens.

## Future of Duniter
Until now, Duniter has been mainly confined to French-speaking users, but it is opening up to the outside world by translating its documentation into English and Spanish and by [rewriting its core](@/blog/2022-04-21-duniter-substrate.md) in the Substrate framework. This will facilitate interoperability with other blockchains and the extension of its web of trust, one of its main strengths.

If you want to know more about Duniter or want to contribute, please get in touch with us on our Discourse technical forum.

<span class="center-content">
<a href="https://forum.duniter.org/" class="w3-round w3-button w3-blue"><img src="/img/duniter_forum.png"/> Get in touch!</a>
</span>
