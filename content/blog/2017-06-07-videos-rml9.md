+++
title = "RML9 : videos of the technical days"
date = 2017-06-07

[extra]
thumbnail = "/PELICAN/images/play.svg"

[taxonomies]
authors = ["cgeek",]
tags = ["RML",]
category = ["Events",]

[extra.translations]
fr = "blog/videos-rml9/"
+++

# RML9 : videos of the technical days

## Videos of the two technical days are now available!

Watch back at your own pace the tutorials to develop plugins Sakia, Cesium and Duniter.

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL0UDqLtXevvH2GRaD5-HUPWExVxY_MuwK" frameborder="0" allowfullscreen></iframe>

You will also find the interventions of:

* Moul about the use of Silkaj and the HTTP API of Duniter
* Turtle about [paperwallet](https://duniter.tednet.fr/paperwallet/)
* Gpsqueeek for the installation of Duniter on an Internet brick
* Eloïs which presents its specialized node [duniter-currency-monit](https://github.com/duniter/duniter-currency-monit)
* Kimamila which presents the new features of Cesium

![A Paperwallet](/PELICAN/images/rml9/paperwallet.png)

## RML10 in Montpellier

Rendez-vous for the next technical interventions of [**RML10 from 16 to 19 NOVEMBER 2017 in MONTPELLIER**](https://rml10.duniter.org/)!

