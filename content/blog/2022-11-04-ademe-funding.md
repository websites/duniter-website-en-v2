+++
title = "Ğecko and Duniter are receiving funds from ADEME"
date = 2022-11-04
description = """ADEME, which is the French Public Agency for Ecological Transition, launched a call for projects on the theme of "the commons" in March 2021. Their goal is to foster the resilience of local communities via the funding and support of collaborative projects that have an impact on a local level. Duniter answered their call successfully."""

[extra]
thumbnail = "/blog/logoademe2020_rvb-150x150.webp"

[taxonomies]
authors = ["MissSophie",]

[extra.translations]
fr = "blog/financement-ademe/"
+++


# Ğecko and Duniter are receiving funds from ADEME

ADEME, which is the French Public Agency for Ecological Transition, launched a call for projects on the theme of "the commons" in March 2021. Their goal is to foster the resilience of local communities via the funding and support of collaborative projects that have an impact on a local level. The methodology for this call for projects has been designed to be in line with the theme of the commons. A wiki has been created by ADEME ([wiki.resilience-territoire.ademe.fr](https://wiki.resilience-territoire.ademe.fr/)) as well as a dedicated forum ([forum.resilience-territoire.ademe.fr](https://forum.resilience-territoire.ademe.fr/)). This allowed to identify in full transparency the well enough structured projects that met the ADEME requirements for grassroots and local impact as well as for the project's resilience. 

The [Axiom Team](https://axiom-team.fr/) association, whose mission is to support the technical development of the Ğ1 currency, responded to this call. The dossier sent was asking for funds to support the Ğecko application development in order to allow faster payments on local Ğmarkets. It was sent before the decision was made by the development team to work on the [Duniter migration to Substrate](@/blog/2022-04-21-duniter-substrate.md), so the funds are not specifically dedicated to this major migration project. Although Duniter is the core of G1 currency, it makes sense that ADEME funds Ğecko's development as it has a more obvious impact on local communities. 

<span class="center-content"><img alt="illustration" src="/blog/ademe-finance-gecko.png" width="50%"/></span>

Among 247 commons projects listed on ADEME's wiki, Ğecko was chosen to receive funding. You can see the case details [on the wiki](https://wiki.resilience-territoire.ademe.fr/wiki/%C4%9Eecko) or read the summary here:

## Project purpose 
The substantial development of the Ğ1 economy makes it necessary now to improve its digital tools so that it does not hinder its adoption. Ğecko project goals are:

- seamless connection to the network: no need for users to choose a node
- faster payment by simple scanning, as the transaction amount can be embedded in the QR code
- fast payment confirmation: no fork or out-of-sync node
- stronger security via PIN code: no endless identifiers to enter to pay
- higher load capacity: no saturated nodes

These improvements should largely ease and smooth out exchanges, especially during Ğmarkets. Of course, this still relies on a good internet connection, so in white areas, users will have to use paper money or other means of payment.

## On the Duniter side
While some of these improvements will be made on the client software side, most of them require big modifications on the server side. That is why the Ğecko project also aims to develop features in Duniter. Ideally, we would complete the Ğecko project by the time of the Ğ1v2 release. The Ğ1v2 launch has already been announced during the G1 Currency Summer School 2022.

## Budget 
The project cost has been estimated at 50,000 euros, part of which is earmarked for Ğecko frontend and part for the necessary backend features for Ğecko (Duniter, indexers, off-chain data...). ADEME's financial support will cover 70% of this budget, i.e. €35,000, the rest being provided by self-financing. This part of self-financing can be accounted for in money or in kind (for example by valuing volunteer work hours).

## Volunteering 
Although it's important to value volunteer work, its precise estimation is tricky. Several thousand hours of skilled labor were needed to get to Ğecko's current state (more than the total amount of this new project) and that's not including the many hours of self-taught learning nor Duniter-side work. In addition, most of the volunteer work is done on free time, on the side of a paid job, i.e. in the evenings and on weekends. For these reasons, we need to to proceed with financing this work in debt currency (euros) so as to get what volunteering and financing in Ğ1 can't provide: full time contributors.

## Crowdfunding for self-financing 
For all the reasons mentioned above, we will launch a crowdfunding with the Ğ1 community. The goal is to help those among our volunteer technical contributors who wish to do so to get free from their "euro job" and to use their full time for this project. The sponsor structure will be Axiom Team, which brings together most of the developers, and the method of sharing the total donations will be decided by the method of distribution circles.

---

To keep up to date with funding news, subscribe to the "Axiom Team" category ([in French](https://forum.monnaie-libre.fr/c/associations/axiom-team/115)) on the Monnaie Libre Forum (G1 currency Community Forum)!