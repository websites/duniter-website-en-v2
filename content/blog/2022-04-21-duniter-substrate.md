+++
title = "Duniter v2 with Substrate 🏗"
date = 2022-04-21
description = "Duniter version 2 will be using Substrate framework. Here is why."

[extra]
thumbnail = "/img/duniterv2.svg"

[taxonomies]
authors = ["HugoTrentesaux",]


[extra.translations]
fr = "blog/duniter-substrate/"
+++

# Duniter v2 with Substrate 🏗

[TOC]

## A necessary and beneficial evolution 👍

Duniter software has seen many evolutions since its launch [in April 2016](@/blog/2016-04-24-ucoin-rename-duniter.md), and so have blockchain technologies. While at the time a "home-made" solution was relevant, we believe that it is now better to base our software on widely tested and proven bricks to ensure scalability. We found the [Substrate](https://substrate.io/) blockchain framework to be particularly well suited to our needs. Indeed, its modular architecture by "[pallet](https://docs.substrate.io/v3/getting-started/glossary/#pallet)" will let us assemble bricks developed by professionals and focus on what makes Duniter unique: its web of trust and the monetary creation by Universal Dividend (UD).

### Migrating the Ğ1 🏆

Duniter's first objective is to power the Ğ1 currency whose web of trust currently counts 4971 members with a 300 members / month growth rate. We want to work together with the community to make the transition towards Duniter v2 as smooth as possible without a "hard fork". Each member will keep his/her status, each wallet will keep its balance, the currency parameters will remain nearly unchanged, wallets (formerly "clients") software will be adapted, and we will try to make the transition as short as possible (ideally less than a day).

### International outreach 🌍

Unlike the "home-made" solution documented mainly in French, Substrate is opening us to an international community of developers. Duniter will bring what the cryptocurrency world is missing: a solid and sound monetary theory. Conversely, the cryptocurrency world will provide Duniter with the expertise needed to take the next step in experimenting with a free currency. To make understanding easier, we will be using a precise vocabulary [(fr)](https://forum.duniter.org/t/vocabulaire-de-base-pour-comprendre-duniter-v2s-lecture-fortement-recommandee-pour-tous/9053) already well established in the domain.

### Blockchain as a common resource 🌲

[🔗 forum (fr)](https://forum.duniter.org/t/comment-partager-equitablement-cette-ressource-commune-quest-la-blockchain-g1/9050/)

Duniter v1 was already part of a comprehensive approach to treat blockchain as a **[common](https://en.wikipedia.org/wiki/Commons)**, <q> a resource that community manages for individual and collective benefit </q>. The migration to Substrate is an ideal opportunity to push the thinking further, by introducing *on-chain* governance tools for collective decisions (voting, protocol evolution) and the allocation of finite blockchain resources (ex. spam resistance).

## Some technical points 👩💻

Let's now move on to the technical part, where we deal with the concrete issues raised by this migration.

### Dropping the PoW mechanism in favor of BABE/GRANDPA 👾

[🔗 forum (fr)](https://forum.duniter.org/t/abandon-de-la-pow-au-profit-de-babe-grandpa/8901) [🔗 forum (fr)](https://forum.duniter.org/t/g1v2-proposition-de-passer-au-consensus-hybride-babe-grandpa/8610)

Duniter's proof of work was often a criticised point. Although our custom [personalised difficulty](@/wiki/contribute/archive/duniter-proof-of-work.md#personalised-difficulty) which reduces the energy consumption and distributes the addition of blocks among blacksmiths, the [BABE](https://docs.substrate.io/v3/advanced/consensus/#babe) for block authoring and [GRANDPA](https://docs.substrate.io/v3/advanced/consensus/#grandpa) for finalization will allow us to greatly accelerate the data validation (~30 minutes → ~30 seconds) while limiting the "waste" of CPU resources.


### Blacksmiths subweb 🌋

[🔗 forum (fr)](https://forum.duniter.org/t/la-sous-toile-forgerons/9047)

In Duniter v1, any member of the web of trust is given the rights to create the UD, certify, and add blocks all at once. This is a major security flaw because by hacking into 30 poorly secured accounts it is possible to take control of the entire blockchain. With the increasing number of users, it is better to decouple different rights to grant block authoring only under security conditions that cannot be imposed on the general public.

