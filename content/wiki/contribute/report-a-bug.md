+++
title = "Report a bug"
date = 2017-03-27
weight = 9

[taxonomies]
authors = ["cgeek",]

[extra.translations]
fr = "wiki/contribuer/rapporter-un-bug/"
+++

# Report a bug

You may be able to detect a bug on one of Duniter's applications! If so, **please write a bug report**, as explained below.

### About GitLab

This is the preferred method. *Really, we prefer this one*: **this is our official list of bugs**.

If you are registered on our GitLab instance, simply go on the good repositories:

* [Duniter tickets repository](https://git.duniter.org/nodes/typescript/duniter/issues/)
* [Sakia tickets repository](https://git.duniter.org/nodes/typescript/duniter/issues/)
* [Cesium tickets repository](https://git.duniter.org/clients/cesium/cesium/issues/)
* [Silkaj tickets repository](https://git.duniter.org/clients/python/silkaj/issues/)

Then put a title, and a description about the encountered error:

![Screenshot of a new github issue](/PELICAN/images/contribuer/ticket_github.png)

> **/!\ Do not forget to validate by clicking the "Submit new issue" button!**

### On the forum

If you're really struggling to create a ticket on GitLab (it's a good opportunity to learn!), You can also [create a topic on our forum](https://forum.duniter.org/):

![Screenshot of a new topic on the forum](/PELICAN/images/contribuer/ticket_forum.png)

Pleas, make sure you to:

* Give a title
* Give a description
* Select "support" category

Then, validate. Someone should answer you, then *we will register your problem* on GitLab. Thanks for reporting your issue, that’s the most important!

