+++
title = "Architecture"
date = 2017-10-09
weight = 10

[taxonomies]
authors = ["vit",]

[extra.translations]
fr = "wiki/contribuer/blockchain-nodejs/architecture-duniter/"
+++

# Architecture

## Clients network architecture

Here is a glimpse of the architecture of Duniter between one server and clients.

![uml](/PELICAN/uml/00182988.svg)
> [uml code](/PELICAN/uml/00182988.svg.txt)
