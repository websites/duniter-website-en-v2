+++
title = "Fund the project"
date = 2024-06-12
weight = 5

[taxonomies]
authors = ["HugoTrentesaux",]

[extra.translations]
# fr = ""
+++

# Fund the project

The most coherent way of funding the project is to use a *libre* currency! Several accounts (here base 58 pubkeys) are meant to direct Ğ1 funding:

- `78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8` official fund for the contributors to the Duniter/Ğ1 technical ecosystem
- `TENGx7WtzFsTXwnbrPEvb6odX2WnqYcnnrjiiLvp1mS` remuniter, a fund to compensate smith for contributing to the network

## Other cryptocurrencies

We do not have yet shared multisig accounts in other cryptocurrencies. You can help us set up one on the forum or directly contact a developer to fund him/her directly.

## Fiat currencies

Duniter accepts payment through:

- liberapay [https://en.liberapay.com/duniter](https://en.liberapay.com/duniter)
- [that's all for the moment]

But Duniter is an open collective, not an organization that can hire developers. If you want to fund Duniter indirectely, you can fund support organizations like:

- Axiom Team [https://axiom-team.fr/](https://axiom-team.fr/)