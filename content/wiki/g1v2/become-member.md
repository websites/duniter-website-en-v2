+++
title = "Become member"
weight = 20
+++

# Become member

Once you are familiar with Ğ1 accounts and transfers and know some other Ǧ1 members <abbr title="In Real Life">IRL</abbr>, it's time to create the Universal Dividend by gaining membership. For that you will have to complete the process detailed in this page.

1. read the [Ğ1 license](@/wiki/g1/license-txt.md)
1. select an account with well secured secret to bear the identity
1. get a first certification
1. choose a pseudo
1. get other certifications
1. comply with the distance rule

## Ğ1 license

The license is a contract that help preserve the [common good](https://en.wikipedia.org/wiki/Common_good) that is the web of trust. Complying with it ensures the identity uniqueness and then the trust everybody can have in the system.

[read it](@/wiki/g1/license-txt.md)

## Select an account

The account you choose will be the one perceiving the UD and emitting certifications. It has to be well secured so that no attacker can be usurping your identity.
Bear in mind that the blockchain is a public system and that all transactions of this account which were previously anonymous will then be linked to your pseudonym.

## Get a first certification

By certifying you, a Ğ1 member should ensure you have read the Ğ1 license and trust you for not creating multiple identities. You can give him your account public address and exchange some Ğ1 with him to show that you actually have control over the account. You have 2 days to name your identity after that.

## Name your identity

In Duniter, each identity is uniquely associated with a pseudo that you can choose once and never change later. This is a public human-readable trust factor written in blockchain and used everywhere in the apps. It must obey the following rules:

- pseudo is not already used
- length must be greater than or equal to 3 characters
- length must be lesser than or equal to 42 characters
- all characters are alphanumeric or `-` or `_`

You can choose anything you wish that represents you and want to be associated with your IRL identity. Some recommandations:

- use your name if you want it public, otherwise don't and use a pseudonym instead
- keep in mind that it can not be changed and thus should not include things that can change and become outdated (like your department number)
- make it simple for people to read as it will be read often in the future

The only way to change pseudonym is to revoke your identity and start from scratch, which is a long process, so do not rely on that and choose wisely.

## Get other certifications

Same as getting the first one, Ğ1 members should make sure you have read and understood the Ǧ1 licence and trust you. But this time, your identity is named. You have 2 month to become member.

## Comply with the distance rule

In most cases, 5 certifications is enough to enter the web of trust, and the fifth will trigger evaluation of distance rule, which will make your identity member 2h later. But if certifications are issued by ill-connected members, there is a chance that you do not comply with the distance rule and need other certifications.
You then would have to request distance evaluation again.
Falsy distance evaluation will result in a slash going to [treasury](@/wiki/g1v2/treasury.md).

## Be member

Once you are member of the Ǧ1 web of trust, you gain these rights:

- perceive Universal Dividend
- emit certifications
- [link an account to your identity](@/wiki/duniter-v2/doc/fees.md#account-linking) to get refunded from transaction fees

Make good use of it!

## Stay member

To stay member, you will have to keep a certification count above 5 (certifications expire after two years) and to renew your membership (membership expires after 1 year). To renew your membership, you have to comply again with the distance rule.

## Auto-revokation

If your identity loses membership (by passing below received certification count threshold or not renewing), you have a 1 year delay to become member again before the identity is revoked automatically. A revoked identity prevents from using the same pseudo forever.