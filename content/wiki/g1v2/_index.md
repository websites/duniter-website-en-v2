+++
title = "Ğ1 currency (v2)"
weight = 40
+++

{% note(type="info") %}
This documentation is about Ğ1v2, the Ğ1 network running on Duniter v2 with data coming from Ğ1v1 network.
{% end %}

Ğ1 is the first free or *libre* currency in the sense of <abbr title="Relative Theory of Money">[RTM](https://en.trm.creationmonetaire.info/)</abbr>. It is powered by Duniter software. Anyone is free to use the ğ1, all they have to do is create an account on a wallet such as Cesium or Ğecko and start exchanging. To create ğ1s, you need to join the web of trust, a decentralized identity system ensuring everyone creates an equal share of currency. To do this, you have to read the [Ğ1 Licence](@/wiki/g1/license-txt.md) and commit to abide by it with five june creator members.

You can find technical information on using the Ğ1 here. For monetary and societal aspects, please refer to the [g1currency.org](https://g1currency.org/) website.

* [Ğ1 license](@/wiki/g1/license-txt.md)
* [create an account](@/wiki/g1v2/create-account.md)
* [become member](@/wiki/g1v2/become-member.md)
* [oneshot account](@/wiki/g1v2/oneshot-account.md)
* [treasury](@/wiki/g1v2/treasury.md)


