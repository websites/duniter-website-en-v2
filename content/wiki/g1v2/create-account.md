+++
title = "Create an account"
weight = 10
+++

# Create an account

To create an account, you first have to generate you key pair in the app of your choice. You will get two things:

- a **secret** you are the only owner of
- a **public address** for your account in the "SS58" format

Store your secret securely, if you lose it, you will never be able to recover your account, and all funds on it will be lost.

## Receive Ğ1

Share your public address with someone who will send you your first Ğ1 to begin with (it can be a gift or a payment for a service you offered).

### Existential deposit

The minimum amount allowed on an account is 1 Ğ1. It should never go below this threshold (otherwise the remaining will be sent to the [treasury](@/wiki/g1v2/treasury.md)). This prevents too many accounts to exist at the same time and saturate the blockchain.

## Send Ğ1

Once your account has Ǧ1 on it, you can send some to another account whose address you know by issuing a transfer.

### Transaction fees

{% note(type="info", markdown=true) %}
A blockchain is a [common good](https://en.wikipedia.org/wiki/Common_good). In order to protect it from ill-intentioned people, some rules are defined collectively to ensure everybody can benefit from it. 
{% end %}

The transfer operation has a computational cost. Anonymous accounts pay a fee for that. The order of magnitude of fees are 0.07 Ğ1 per transaction.

To get *free of charge* transactions, you have to link your account to an identity by joining the web of trust. Read [become member](@/wiki/g1v2/become-member.md) for that.