+++
title = "About"
# date = 2017-03-27
weight = 80

# [taxonomies]
# authors = ["cgeek",]
# tags = ["about",]

[extra.translations]
fr = "wiki/about/"
+++

# About

## License

All the content of this site is under [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) license except where said otherwise.

Its code is fully available on a GitHub repository [git.duniter.org/websites/duniter-website-en-v2](https://git.duniter.org/websites/duniter-website-en-v2) with the instructions for reproducing it on your machine.

## Images used on this site

The offical Duniter / Ğ1 logos are on this repo : [https://git.duniter.org/communication/g1](https://git.duniter.org/communication/g1).  
Some logos and images are the production of other people, you can find their origin on the [Credits](@/wiki/about/credits.md) page.

## Who are we?

We are simple citizens, mostly french people, trying to produce a new economic value nammed « libre currency », of digital form, and respecting the principles established by the [Relative Theory of Money (RTM)](http://en.trm.creationmonetaire.info/)

There is currently no official organization behind Duniter, even if the project is supported bien the *Art et Zerty* company, French EURL owned by the Duniter software founder Cédric Moreau a.k.a *cgeek*, and owning the Duniter tradmark in France.

If the project takes off on long term, the creation of a Foundation is already considered by its initiators. The trademark would then be transfered to the Foundation.

## Contact

Please get in touch on the [duniter forum](https://forum.duniter.org/).
