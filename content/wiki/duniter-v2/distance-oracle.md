+++
title = "Distance oracle"
date = 2024-02-23
weight = 46

[taxonomies]
authors = ["HugoTrentesaux",]
+++

# Distance oracle in separate compose file

In Duniter v2, the distance computation is managed outside the runtime core to avoid taking all the block capacity. Running a distance oracle is technically optional but required for the web of trust health. If too few smith run a distance oracle, some distance evaluations requests could have no response, preventing identities to join the web of trust or renew their membership.

Here is an example on how to run the distance oracle outside the validator node using docker networks and docker volumes.

```yml
# Duniter distance oracle plugging to smith node
# distance oracle uses same image but another entrypoint
services:
  distance-oracle:
    image: duniter/duniter-v2s-gdev:latest      # <--- same image as Duniter
    entrypoint: docker-distance-entrypoint      # <--- but an other entrypoint
    environment:
      ORACLE_RPC_URL: "ws://duniter-smith:9944"   # <--- here "duniter-smith" corresponds to your smith service
      ORACLE_RESULT_DIR: "/var/lib/duniter/chains/gdev/distance/" # <--- "gdev" or the name of the chain
      ORACLE_EXECUTION_INTERVAL: "200"         # <--- frequency should be adjusted on network
    volumes:
      - data-smith:/var/lib/duniter             # <--- uses the docker volume of your smith node
    networks:
      - duniter                                 # <--- uses the docker network of your smith node

# external volume of duniter node to share data for the inherent to read
volumes:
  data-smith:
    name: duniter-gdev-smith_data-smith         # <---- volume name of your smith node
    external: true

# external network of duniter node to read data from rpc API
networks:
  duniter:
    name: duniter-gdev-smith_default            # <---- network name of your smith node
    external: true
```

It is convenient to run it aside if you want more control, for example to be able to update it independantly from the smith node. To know the name of the volume and the network, you can do:

```sh
# get the name of the volume
docker volume ls
# get the name of the network
docker network ls
```

They are defined by default by the name of the folder containing the `docker-compose.yml`.