+++
title = "Runtime storages"
weight = 101

[extra]
EXTERNAL_CONTENT = "https://git.duniter.org/nodes/rust/duniter-v2s/-/raw/master/docs/api/runtime-storages.md"
auto_toc = true
+++

⚠️ the content of this file will be overwritten by EXTERNAL_CONTENT