+++
title = "Fees"
weight = 40
+++

# Fees

## Account linking

Account linking is a new feature of Duniter v2 allowing to link a standard account (not oneshot account) to an identity. Any number of accounts can be linked to an identity.

- see [`identity.link_account(account_id, payload_sig)`](/wiki/duniter-v2/runtime-calls/#link-account-8)
- and [`account.unlink_identity()`](/wiki/duniter-v2/runtime-calls/#unlink-identity-0)

The payload to sign is the following concatenated:

- `LINK_IDTY_PAYLOAD_PREFIX` = `link`
- `genesis_hash`
- `idty_index`
- `account_id`

## Applications

The first application is the one it was introduced for: use the quota system. Transaction fees will still be paid, but will be refunded using quotas if the account is linked to an identity.

Other applications can be developed outside the blockchain like using this information as an onchain proof of account ownership.