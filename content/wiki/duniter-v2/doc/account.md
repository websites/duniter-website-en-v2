+++
title = "Account"
weight = 10
+++

# Account
The definition of an "account" can differ considering the client (app, wallet) or server (blockchain) point of view.

## Client side
From the blockchain perspective, only keys and signatures are visible. However, when "creating" and account client-side, the app developer can comply with some conventions which allow for secret interoperability between clients.

### Secret management and key derivation
Secret has to be generated with a good enough entropy so that nobody can generate the same secret again by chance. For that you should use only audited dedicated software and not rely on custom implementation. Human are generally bad to generate randomness, so custom secret should only be available for advanced used who know what they are doing.

We recommand using a BIP39-like implementation which presents the secret to the user in the form of a mnemonic. TODO describe this spec somwhere. WARN the BIP39 spec is broken and should not be used as-is. To see why, read <https://forum.duniter.org/t/polkadot-js-multilang-mnemonic/10132/48>.

### Derivations
We recommand to use BIP32 derivation scheme. Some apps scan the first integer-path derivations like `//0`, `//1`, `//2` and so on for "multi account secret" feature. TODO tell more about derivations.

### Crypto scheme
Crypto scheme supported by Duniter blockchain are: ed25519, sr25519, ecdsa. The default and recommanded is sr25519. User should be informed in case an other scheme is used.

### Legacy format
TODO describe the legacy scrypt KDF.

## Server side
The account can exist server-side on blockchain storage if it meets existence conditions.

### Existential deposit
Existential is the only necessary condition for account existance. It is available as a runtime constant:

`constants.balances.existentialDeposit()`

### Reference counting
TODO document refcounting (providers / consumers / sufficients)

### Example

Duniter account system is very close to substrate Balance pallet accounts.

```
storage.system.account(AccountId)
{
  nonce: 560
  consumers: 1
  providers: 2
  sufficients: 1
  data: {
    free: 2,969,655,031
    reserved: 0
    feeFrozen: 0
    linkedIdty: 344
  }
}
```

nonce, and reference counting (consumers, providers, sufficients) come from Substrate `AccountInfo`. While `data` comes from Duniter's `AccountData`. 

### Balance
TODO document free/reserved...

### Linked identity
See [account linking](@/wiki/duniter-v2/doc/fees.md#account-linking) in fees.