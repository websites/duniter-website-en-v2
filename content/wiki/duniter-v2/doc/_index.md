+++
title = "Developer guide"
weight = 10
sort_by = "weight"
+++

# Developer guide

This documentation is written for app developers or advanced users. You should first be comfortable with business process explained in [end-user documentation](@/wiki/g1v2/_index.md). You might also need to look deeper and read lower level technical documentation [ipns://doc.duniter.org/](https://doc-duniter-org.ipns.gyroi.de/duniter/). This documentation expects you are familiar with some Substrate framework concepts like pallets, extrinsics, storage, so read [substrate documentation](https://paritytech.github.io/substrate/master/frame_system/pallet/index.html) first. 

## Business logic

These sections aim to explain how to implement general features of Duniter business logic.

- [account management](@/wiki/duniter-v2/doc/account.md) (client and server sides)
- [web of trust](@/wiki/duniter-v2/doc/wot.md) (identity creation, certifications, membership, distance rule)
- [universal dividend](@/wiki/duniter-v2/doc/universal-dividend.md) (unclaimed UDs, UD history)
- [fees and account linking](@/wiki/duniter-v2/doc/fees.md) (fee prediction, use of quota)
- [transaction comments](@/wiki/duniter-v2/doc/transaction-comments.md)

Don't hesistate to test on dev networks using other tools to get a better sense on how this works.

## API specs

An app developer should be able to do everything with constants, storage items, calls, events, and errors, available in the runtime. The runtime is made both from Duniter pallets and Substrate Frame pallets. You will find all these pallets items documented here:

- [runtime constants](@/wiki/duniter-v2/doc/runtime-constants.md): constants like the value of existential deposit
- [runtime storage](@/wiki/duniter-v2/doc/runtime-storages.md): all on-chain storage available for every block
- [runtime calls](@/wiki/duniter-v2/doc/runtime-calls.md): functions that modify state
- [runtime events](@/wiki/duniter-v2/doc/runtime-events.md): events describing what happens on chain
- [runtime errors](@/wiki/duniter-v2/doc/runtime-errors.md): errors returned by functions

If you need to have a deeper look on Duniter pallets, read the Rust documentation: [ipns://doc.duniter.org/](https://doc-duniter-org.ipns.gyroi.de/duniter/).

## Indexer

Performing all the checks in the onchain storage is possible, but sometimes inconvenient. The storage is indeed modeled according to the runtime needs which are sometimes not aligned with client needs. It can be more convenient and sometimes necessaru to use an indexer like Duniter-Squid. The app developer should however keep in mind that:

- the indexer does not brings the garantees of a light client (storage proofs and so)
- when doing that you rely on the indexer and must trust it

Explore the GraphQL schema of Duniter-Squid here: <https://git.duniter.org/nodes/duniter-squid/-/blob/main/schema.graphql>.