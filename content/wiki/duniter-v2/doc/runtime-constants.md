+++
title = "Runtime constants"
weight = 100

[extra]
EXTERNAL_CONTENT = "https://git.duniter.org/nodes/rust/duniter-v2s/-/raw/master/docs/api/runtime-constants.md"
auto_toc = true
+++

⚠️ the content of this file will be overwritten by EXTERNAL_CONTENT