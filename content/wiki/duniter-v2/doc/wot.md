+++
title = "Web of trust"
weight = 20
+++

# Web of Trust

You will learn here how to develop an app that interacts with the web of trust.

[TOC]

## Use-case "Become a member"

For more general public documentation, see [become member](@/wiki/g1v2/become-member.md). You will find below a technical description of what to implement in a client.

### Main scenario

Eve wants to become a member. Her first certifier is Alice.

![image](/uml/become-member.svg)

> download [UML source](/uml/become-member.puml) for this scheme

### Detailed calls and storage items

Some expression used everywhere:

- current block
    - `storage.system.number()`
- existential deposit
    - `constants.balances.existentialDeposit()`

#### Identity creation 

Identity creation is a sensitive step for the web of trust and the apps must make sure that the user is aware of the rules.

- Alice is Member
    - `storage.identity.identities(AliceIndex).status` is `Member`
- EveAccount exists with minimum amount of 2 ĞD (existential deposit plus fee buffer)
    - `storage.system.account(EveAccount).data.free` is higher than `200`
- EveAccount is not already used by an identity
    - `storage.identity.identities(EveAccount)` is `None`
- Alice is ready to create an identity and emit cert 
    - `storage.identity.identities(AliceIndex).nextCreatableIdentityOn` is lower than current block
    - `storage.certification.storageIdtyCertMeta(AliceIndex).nextIssuableOn` is lower than current block
    - `storage.certification.storageIdtyCertMeta(AliceIndex).issuedCount` is lower than `constants.certification.maxByIssuer()`
- Alice creates identity for Eve, which automatically triggers a certification
    - `call.identity.createIdentity(EveAccount)`
    - `event.identity.created(EveIndex)`
    - `event.certification.certAdded(AliceIndex, EveIndex)`

#### Identity confirmation 

Confirming the identity by choosing a name is a critical part of user's life as he will never be able to change it and the name will be reserved for a long period. 

- Eve is Unconfirmed
    - `storage.identity.identities(EveIndex).status` is `Unconfirmed`
- choosen name meets criteria defined in [`validate_idty_name`](https://doc-duniter-org.ipns.gyroi.de/duniter_primitives/fn.validate_idty_name.html)
    - length lower than 42
    - only alphanumeric or `-` or `_`
- choosen name is available
    - `storage.identitiesNames(EvePseudo)` is `None`
- Eve confirms her identity by naming it with a pseudo
    - `call.identity.confirmIdentity(EvePseudo)`
    - `event.identity.idtyConfirmed(EveIndex, EveAccount, EvePseudo)`

#### Other certifications (before entry)

- Eve is Unvalidated
    - `storage.identity.identities(EveIndex).status` is `Unvalidated`
- Bob is Member
    - `storage.identity.identities(BobIndex).status` is `Member`
    - `storage.certification.storageIdtyCertMeta(BobIndex).nextIssuableOn` is lower than current block
    - `storage.certification.storageIdtyCertMeta(BobIndex).issuedCount` is lower than `constants.certification.maxByIssuer()`
- Bob certifies Eve
    - `call.certification.addCert(EveIndex)`
    - `event.certification.certAdded(BobIndex, EveIndex)`

#### Identity validation

Identity is "validated" on the first time it acquires membership. Joining the web of trust requires evaluation of distance criteria and is done automatically after a positive evaluation. Since this computation is costly, it is done offchain in the distance oracle. Results are reported by validators through an inherent.

When the last certifier knows the distance rule will be ok, he can trigger distance rule evaluation in a batch call with its certification. He risks slashing of `constants.distance.evaluationPrice()` if the distance is actually evaluated negatively. Alternatively Eve can request distance evaluation for herself.

##### Targeted distance evaluation request

- distance rule has been evaluated positively locally on web of trust at block `storage.distance.evaluationBlock()`
    - see distance rule detailed article (TODO)
- Anonymous requests distance evaluation for Eve
    - `call.distance.requestDistanceEvaluationFor(EveIndex)`
    - `event.distance.evaluationRequested(EveIndex, AnonymousAccount)`
- distance evaluation has been added to queue
    - `storage.distance.pendingEvaluationRequest(EveIndex)` is `Some(AnonymousAccount)`
- on next evaluation period, distance oracles start producing results on validator nodes
    - this is invisible on the blockchain
- on next evaluation period, validators start to include their evaluations through inherents
    - `call.distance.updateEvaluation(Result)`
- at the beginning of evaluation period, the median of results is taken and events are triggered
    - `event.distance.evaluatedValid(EveIndex, DistancePercentage)`
    - `event.identity.identityValidated(EveIndex)`
    - `event.membership.membershipAdded(EveIndex, ExpireBlock)`

## Staying member

As an app developer, if you manage identities, you should make sure that the user is aware of membership expiration and renews his membership in time.

### Renewing certs

Certifications expire after `constants.certification.validityPeriod()` (number of blocks). They can be re-created after expiration, or renewed while active. There is no need to renew certification too long before expiry as it trigger the certPeriod delay.

- Bob renews his certification to Eve
    - `call.certification.reneweCert(EveIndex)`
    - `event.certification.certRenewed(BobIndex, EveIndex)`

### Renewing membership

If Eve is still "alive", she has to renew her membership to stay member of the web of trust. There is no need to renew membership too long before expiry.

- Eve renews her membership by requesting distance evaluation
    - `call.distance.requestDistanceEvaluation()`
- [...] *same steps as distance evaluation*
- Eve membership is renewed after positive distance evaluation
    - `event.distance.evaluatedValid(EveIndex, DistancePercentage)`
    - `event.membership.membershipRenewed(EveIndex, ExpireBlock)`