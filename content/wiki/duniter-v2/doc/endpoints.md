+++
title = "Endpoints"
weight = 5
+++

# Endpoints
The best approach is to build your app around a light client which uses p2p connections. When this is not easy (e.g. mobile client), you can rely on external endpoints. These endpoints are listed on the "networks" repo for `gdev`, `gtest` and `g1` networks: <https://git.duniter.org/nodes/networks>.

When connecting to a remote endpoint, a synchronization check should be done. TODO explain

## Duniter RPC
TODO document duniter wss rpc api

## Duniter-Squid GraphQL
TODO document duniter-squid graphql api
