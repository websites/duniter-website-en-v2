+++
title = "Duniter v2"
# date = 2023-01-22
weight = 20
sort_by = "weight"
insert_anchor_links = "right"

# [taxonomies]
# authors = ["HugoTrentesaux",]

[extra.translations]
fr = "wiki/doc-v2/"
+++

# Duniter v2

Duniter v2 is a rewrite of Duniter v1 in the substrate framework with the goal of migrating the Ğ1 currency. It is now under development and needs testing. You will find here user documentation extracted from [Duniter git repository](https://git.duniter.org/nodes/rust/duniter-v2s/) as well as general explanation.

- dev documentation [ipns://doc.duniter.org/](https://doc-duniter-org.ipns.gyroi.de/duniter/)
- git repository [git.duniter.org/nodes/rust/duniter-v2s/](https://git.duniter.org/nodes/rust/duniter-v2s/)
- dedicated forum sections:
  - development [forum.duniter.org/c/dev/duniter-v2s/](https://forum.duniter.org/c/dev/duniter-v2s/57)
  - protocol [forum.duniter.org/c/protocols/g1v2proto/](https://forum.duniter.org/c/protocols/g1v2proto/60)
  - support [forum.duniter.org/c/support/duniter-v2/](https://forum.duniter.org/c/support/duniter-v2/83)

In this section you will find everything needed to start contributing to the network. If you feel like there is something missing, please ask on the forum support section, this helps improving the tutorials and is by itself a valuable contribution!

## Run a mirror node

A mirror node is a node the user connects to through the RPC API. Running a Duniter mirror node is useful for the network's redundancy, it allows to share the load between different servers and increase resilience to failures. This is also a good practice step before forging blocks.

- [run a mirror node](@/wiki/duniter-v2/run-mirror.md) to provide a blockchain endpoint
- [configure your node (docker)](@/wiki/duniter-v2/configure-docker.md) (docker environment variables description)
- [inspect your node in polkadotjs app](@/wiki/duniter-v2/polkadotjs/_index.md) to learn what it looks like
- [run an indexer](@/wiki/duniter-v2/indexers/duniter-squid.md) to provide an indexing endpoint
- [run a datapod](@/wiki/duniter-v2/datapod/_index.md) to host offchain data

A mirror node in *archive* mode is needed to run an indexer.

## Start forging blocks

In [Duniter vocabulary](@/wiki/duniter-v2/glossary.md) "smith" means someone allowed to add blocks to the blockchain. Contrary to Duniter v1, smith is mainly a technical role, the goverance being done *on chain*. The competences needed to become smith are less technical than human. A smith should:

- follow good security practices for his keys and node
- be able to react quickly in case of failure (or go-offline)
- contribute to the smith web of trust by inviting other smith

Here are the steps to become smith:

1. [run a smith node](@/wiki/duniter-v2/run-smith.md) (a node enabled to forge blocks)
1. [become smith](@/wiki/duniter-v2/become-smith.md) (be member of the smith web of trust)

## Develop an app

Whether you contribute to an existing end client like [Ğcli](@/wiki/duniter-v2/gcli/_index.md) or an [Indexer](@/wiki/duniter-v2/indexers/_index.md) or create a new app from scratch, you might be interested to read:

- [guide for app developers](@/wiki/duniter-v2/doc/_index.md)
- generated documentation for [calls](@/wiki/duniter-v2/doc/runtime-calls.md), [events](@/wiki/duniter-v2/doc/runtime-events.md) and [errors](@/wiki/duniter-v2/doc/runtime-errors.md)
- general documentation about the RPC API (TODO)
- graphql schema of the duniter-squid indexer (TODO)

Don't hesitate to reach up in the "clients" forum section [forum.duniter.org/c/clients/](https://forum.duniter.org/c/clients/16).