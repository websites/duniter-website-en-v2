+++
title = "Run a mirror node"
date = 2023-01-23
weight = 10

[taxonomies]
authors = ["HugoTrentesaux",]

[extra.translations]
fr = "wiki/doc-v2/installer/miroir/"
+++

# Run a mirror node

The utility of a mirror node is to expose a RPC endpoint to submit extrinsincs and get blockchain data. It can be an *archive* node if it keeps a copy of the whole storage for each block or a standard node if it enables storage pruning and only keeps state transitions. Both can be used for synchronization but only *archive* nodes can be used to retreive blockchain information *at any given block*.

When you have a functional endpoint, do not forget to share it if you want other users to benefit from it!

[TOC]

## Install

There are multiple installation techniques depending on your technical background and goals. Choose the one you are most familiar with and do not hesitate to request more details if you are struggling. 

### Docker
We chose docker as the standard way to deploy a node. First [install docker](https://docs.docker.com/engine/install/#server) on your platform.

#### Compose file
Then use the following `docker-compose.yml` example file:

```yml
services:
  duniter-mirror:
    image: duniter/duniter-v2s-xxx             # <----
    restart: unless-stopped
    ports:
      # Prometheus (private, for monitoring)
      - 127.0.0.1:9615:9615
      # RPC API (public, through reverse proxy)
      - 9944:9944
      # p2p (public, open port for node to node connections)
      - 30333:30333
    volumes:
      - data-mirror:/var/lib/duniter/
    environment:
      - DUNITER_CHAIN_NAME=yyy                 # <----
      - DUNITER_NODE_NAME=zzz                  # <----
volumes:
  data-mirror:
```

First set the **image**. It tells the version of Duniter you want to use as well as the network you want to connect to (`duniter-v2s-gdev-800` for current gdev network). You can find the available images here: <https://hub.docker.com/u/duniter>.

Then set the **chain name** to the same chain as the image. Replace `yyy` by `gdev`, `gtest` or `g1`.

Finaly choose a **node name** that will help you and other identify your node on the network. Replace `zzz` by a name like `my-name_mirror`.

For more info about how to configure your node using Docker, read [configure (docker)](@/wiki/duniter-v2/configure-docker.md).

#### Start node
When done, in the folder of this `docker-compose.yml` file, run `docker compose up` to start your node. You should see logs like this:

```
[+] Running 6/6
 ⠿ duniter-rpc Pulled                                                      18.1s
   ⠿ 548fcab5fe88 Pull complete                                            14.2s
   ⠿ a6cb4a21b40a Pull complete                                            14.4s
   ⠿ c1e8b5464c78 Pull complete                                            15.6s
   ⠿ abff9d6e5a19 Pull complete                                            15.7s
   ⠿ 0bce2fe9b911 Pull complete                                            15.7s
[+] Running 3/3
 ⠿ Network hugo_default          Created                                    0.0s
 ⠿ Volume "hugo_data-rpc"        Created                                    0.0s
 ⠿ Container hugo-duniter-rpc-1  Created                                    0.1s
Attaching to hugo-duniter-rpc-1
hugo-duniter-rpc-1  | Generating node key file '/var/lib/duniter/node.key'...
hugo-duniter-rpc-1  | 12D3KooWBcWNStGCq1WwWCsfTYKcc76CQ94FYUwbcsPzDWFWToub
hugo-duniter-rpc-1  | Node peer ID is '12D3KooWBcWNStGCq1WwWCsfTYKcc76CQ94FYUwbcsPzDWFWToub'.
hugo-duniter-rpc-1  | Starting duniter with parameters: --name hugo-trentesaux-rpc-albatros --node-key-file /var/lib/duniter/node.key --rpc-cors all --chain gdev -d /var/lib/duniter --unsafe-rpc-external --unsafe-ws-external
hugo-duniter-rpc-1  | 2023-01-23 16:09:41 Duniter    
hugo-duniter-rpc-1  | 2023-01-23 16:09:41 ✌️  version 0.3.0-3189d769f70    
hugo-duniter-rpc-1  | 2023-01-23 16:09:41 ❤️  by Axiom-Team Developers <https://axiom-team.fr>, 2021-2023    
hugo-duniter-rpc-1  | 2023-01-23 16:09:41 📋 Chain specification: Ğdev    
hugo-duniter-rpc-1  | 2023-01-23 16:09:41 🏷  Node name: hugo-trentesaux-rpc-albatros    
hugo-duniter-rpc-1  | 2023-01-23 16:09:41 👤 Role: FULL    
hugo-duniter-rpc-1  | 2023-01-23 16:09:41 💾 Database: ParityDb at /var/lib/duniter/chains/gdev/paritydb/full    
hugo-duniter-rpc-1  | 2023-01-23 16:09:41 ⛓  Native runtime: gdev-400 (duniter-gdev-1.tx1.au1)    
hugo-duniter-rpc-1  | 2023-01-23 16:09:43 🔨 Initializing Genesis block/state (state: 0x4096…cfd1, header-hash: 0xf9bb…0dfc)    
hugo-duniter-rpc-1  | 2023-01-23 16:09:43 👴 Loading GRANDPA authority set from genesis on what appears to be first startup.    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 👶 Creating empty BABE epoch changes on what appears to be first startup.    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 🏷  Local node identity is: 12D3KooWBcWNStGCq1WwWCsfTYKcc76CQ94FYUwbcsPzDWFWToub    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 💻 Operating system: linux    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 💻 CPU architecture: x86_64    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 💻 Target environment: gnu    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 💻 CPU: Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 💻 CPU cores: 4    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 💻 Memory: 15713MB    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 💻 Kernel: 5.15.85-1-MANJARO    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 💻 Linux distribution: Debian GNU/Linux 10 (buster)    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 💻 Virtual machine: no    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 📦 Highest known block at #0    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 〽️ Prometheus exporter started at 127.0.0.1:9615    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 Running JSON-RPC HTTP server: addr=0.0.0.0:9933, allowed origins=None    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 Running JSON-RPC WS server: addr=0.0.0.0:9944, allowed origins=None    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 ***** Duniter has fully started *****    
hugo-duniter-rpc-1  | 2023-01-23 16:09:44 creating instance on iface 172.18.0.2    
hugo-duniter-rpc-1  | 2023-01-23 16:09:45 🔍 Discovered new external address for our node: /ip4/78.199.27.8/tcp/30333/ws/p2p/12D3KooWBcWNStGCq1WwWCsfTYKcc76CQ94FYUwbcsPzDWFWToub    
hugo-duniter-rpc-1  | 2023-01-23 16:09:49 ⚙️  Syncing, target=#587673 (4 peers), best: #3683 (0x3ca6…af6c), finalized #3584 (0x847f…6d18), ⬇ 391.2kiB/s ⬆ 6.3kiB/s    
hugo-duniter-rpc-1  | 2023-01-23 16:09:54 ⚙️  Syncing 724.6 bps, target=#587674 (5 peers), best: #7307 (0x8165…f519), finalized #7168 (0xca18…8675), ⬇ 238.2kiB/s ⬆ 2.9kiB/s    
hugo-duniter-rpc-1  | 2023-01-23 16:09:59 ⚙️  Syncing 689.6 bps, target=#587674 (5 peers), best: #10755 (0x0993…d78e), finalized #10752 (0xd01d…8d6b), ⬇ 237.8kiB/s ⬆ 2.5kiB/s    
hugo-duniter-rpc-1  | 2023-01-23 16:10:02 on_idty_change: pallet_authority_members.change_owner_key(): <wasm:stripped>    
hugo-duniter-rpc-1  | 2023-01-23 16:10:04 ⚙️  Syncing 706.1 bps, target=#587675 (5 peers), best: #14288 (0x12e1…c95e), finalized #13824 (0xf539…7329), ⬇ 237.8kiB/s ⬆ 2.5kiB/s 
```

It downloaded the good docker image, started the node, and started synchronization.

```
⚙️  Syncing 706.1 bps, target=#587675 (5 peers), best: #14288 (0x12e1…c95e), finalized #13824 (0xf539…7329), ⬇ 237.8kiB/s ⬆ 2.5kiB/s 
```

The line above tells you the state of the synchronization. You get the target block of the network, the block your are currently at, and the average sychronization speed (here 706 blocks per second).

Once you are happy with the result, you can safely interrupt Duniter (with `Ctrl+C`) and start it again in background with:

```
docker compose up --detach
```

Then you can [inspect your node in polkadotjs app](@/wiki/duniter-v2/polkadotjs/_index.md) locally or remotely depending on where you started your node.

For more info about how to interact with Duniter through Docker, read [configure (docker)](@/wiki/duniter-v2/configure-docker.md).

### Binary

You can download a pre-build binary (no official support yet) or compile Duniter with:

```bash
# choose the branch corresponding to your target network {gdev, gtest, g1}
git clone https://git.duniter.org/nodes/rust/duniter-v2s.git --branch BRANCH --depth 1
# build duniter binary
cargo build --release
```

This can take ~1h or so depending on your internet connexion and computing power. Then you get a binary in `./target/release/duniter`.

```bash
# read help
duniter --help | less
# start node
duniter --chain=gdev
```

You will find in the help everything you need to configure and run your node.

### Debian

We have a debian package for Duniter v2 currently in test stage. Documentation is available on the repo [https://git.duniter.org/nodes/rust/duniter-v2s/-/blob/master/docs/user/installation_debian.md](https://git.duniter.org/nodes/rust/duniter-v2s/-/blob/master/docs/user/installation_debian.md?ref_type=heads). We give below a more detailed example for a mirror node.

Download debian package from [http://pagu.re/ipfs/QmUyTPcGFs5p1LUVk6c2qqfsTNKzo6wmK9J7Zij9XmDwDi?filename=duniter_0.8.0-1_amd64.deb](http://pagu.re/ipfs/QmUyTPcGFs5p1LUVk6c2qqfsTNKzo6wmK9J7Zij9XmDwDi?filename=duniter_0.8.0-1_amd64.deb) (temporary link, TODO: replace by [https://git.duniter.org/nodes/rust/duniter-v2s/-/releases](https://git.duniter.org/nodes/rust/duniter-v2s/-/releases) when available and non-expiring artifact). Then install and configure it:

```sh
# download with curl
curl https://bafybeidcspicuabhgbzktzigi4jjltf6h4jvly4xf5e7tcfajddi3wh53e.ipfs.pagu.re/ -o duniter_0.8.0-1_amd64.deb
# install with apt
sudo apt install ./duniter_0.8.0-1_amd64.deb
# configure at least DUNITER_NODE_NAME in env file
sudo vim /etc/duniter/env_file
# start the service
sudo systemctl start duniter-mirror.service
# follow the logs to see synchronization in progress
sudo journalctl -u duniter-mirror.service -f
# enable service if you want it to start automatically on system startup
sudo systemctl enable duniter-mirror.service 
```

If you need help, support is available on the forum (currently french only [https://forum.duniter.org/t/paquet-debian-pour-duniter/12327](https://forum.duniter.org/t/paquet-debian-pour-duniter/12327)).

After that, you will be able to use your mirror locally (with [gcli](@/wiki/duniter-v2/gcli/_index.md) or [polkadotjs](@/wiki/duniter-v2/polkadotjs/_index.md)), but for remote use, you will need to expose your node publicly with https, [see below](#expose-the-rpc-api-publicly).

### Yunohost

A Yunohost package is currently available in test stage.

```sh
# install it with
yunohost app install https://git.duniter.org/bgallois/duniter-v2s_ynh
```

For questions, ask on the forum [https://forum.duniter.org/t/paquet-yunohost-pour-duniter/12326](https://forum.duniter.org/t/paquet-yunohost-pour-duniter/12326).

### Other methods

Contributions are welcome for other package like:

- archlinux
- nix
- fedora
- alpine
- ...

## Expose the RPC API publicly

Once you are happy with your node, you can expose publicly its RPC API using a reverse proxy on port 443.

### Nginx reverse proxy

This is an example config file for nginx reverse proxy. You must have installed certificates before.

```conf
server {
  server_name DOMAIN;         # <--- your domain

  listen 443 ssl http2;
  listen [::]:443 ssl http2;

  ssl_certificate /etc/nginx/ssl/DOMAIN.cert;     # <--- location of your cert
  ssl_certificate_key /etc/nginx/ssl/DOMAIN.key;

  root /nowhere;

  add_header X-Frame-Options SAMEORIGIN;
  add_header X-XSS-Protection "1; mode=block";
  proxy_redirect off;
  proxy_buffering off;
  proxy_set_header Host $host;
  proxy_set_header X-Real-IP $remote_addr;
  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header X-Forwarded-Proto $scheme;
  proxy_set_header X-Forwarded-Port $server_port;
  proxy_read_timeout 90;

  location / {
    proxy_pass http://localhost:9944;     # <--- the local port your node is listening on

    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_http_version 1.1;

    proxy_read_timeout 1200s;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $host;
  }
}
```

### Apache reverse proxy

No example yet.

## Share your endpoint with others

Once your endpoint is functional, you can set your apps to use it. And even better, you can share it with other people! The current best way to tell the world to use your node is to add its endpoint to the relevant file in networks repository: <https://git.duniter.org/nodes/networks>. The apps embed this file in their default config and sometimes fetch it for updates at startup.