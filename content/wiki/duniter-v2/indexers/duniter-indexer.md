+++
title = "Duniter indexer"
date = 2023-06-23
edited = 2023-11-24
weight = 55
alias = ["/wiki/duniter-v2/run-indexer/"]

[taxonomies]
authors = ["HugoTrentesaux",]
+++

{% note(type="warning", markdown=true) %}
Duniter-indexer is discontinued, use [duniter-squid](@/wiki/duniter-v2/indexers/duniter-squid.md) instead.
{% end %}

# Duniter indexer

Duniter-indexer is an indexer for Duniter-v2.

- git repository [https://git.duniter.org/nodes/duniter-indexer](https://git.duniter.org/nodes/duniter-indexer)

![schema](https://git.duniter.org/nodes/duniter-indexer/-/raw/master/doc/scheme.svg)
> components of the indexer visible in the README

See how to run an indexer below.

[TOC]

## Requirements

You need a Duniter archive node running on the same server as the indexer.

## Docker compose file

This is an example `docker-compose.yml` file:

```yml
# Duniter indexer

services:
  # postgres database
  postgres:
    image: postgres:12
    restart: always
    volumes:
      - postgres-data:/var/lib/postgresql/data
    environment:
      POSTGRES_PASSWORD: postgrespassword

  # hasura
  graphql-engine:
    image: duniter/hasura-indexer:latest
    depends_on:
      - postgres
    restart: always
    ports:
      - 127.0.0.1:8080:8080 # <--- hasura console will listen on port 8080
    environment:
      # postgres database
      HASURA_GRAPHQL_DATABASE_URL: postgres://postgres:postgrespassword@postgres:5432/postgres
      # enable the console served by server
      HASURA_GRAPHQL_ENABLE_CONSOLE: "true"
      # dev mode
      HASURA_GRAPHQL_DEV_MODE: "false"
      # logging
      # HASURA_GRAPHQL_ENABLED_LOG_TYPES: startup
      # admin password
      HASURA_GRAPHQL_ADMIN_SECRET: hasura_password # <--- you can customize the hasura password here
      # Name of role when the Authorization header is absent in JWT
      HASURA_GRAPHQL_UNAUTHORIZED_ROLE: public
      # telemetry
      HASURA_GRAPHQL_ENABLE_TELEMETRY: "false"

  # indexer (must have duniter in archive mode listening on ws://localhost:9944)
  indexer:
    image: duniter/duniter-indexer:latest
    environment:
      - POSTGRES_HOST=postgres
      - INDEXER_DUNITER_WS_ENDPOINT=ws://duniter-archive:9944 # <--- needs an archive node
      - INDEXER_DUNITER_WS_ENDPOINT_GRAPHIQL=wss://gdev.example.org/ws # <--- customize the ws endpoint the app will be using
      - INDEXER_HASURA_GRAPHQL_ENDPOINT_GRAPHIQL=https://hasura.gdev.example.org # <--- customize the public graphql endpoint
    restart: unless-stopped
    ports:
      - 127.0.0.1:3000:3000
    depends_on:
      # - duniter-archive # depends on archive node through network
      - postgres
      - graphql-engine
    volumes:
      - logs:/logs
      - resources:/resources
    networks:
      - default # allows to connect to postgres database (defined above)
      - duniter # allows to connect to duniter node (defined below)

# define volumes
volumes:
  postgres-data:
  logs:
  resources:

# define duniter external network to allow connect to duniter archive node
networks:
  duniter:
    name: duniter_default # <--- you can tell here the name of the network where lies the duniter-archive node
    external: true
```

As you can see, there are multiple things to configure:

- [ ] the hasura password (otherwise anybody can modify the indexer's data)
- [ ] the public ws endpoint used by the indexer playground
- [ ] the public graphql endpoint used by the indexer playground and users
- [ ] the name of the network of the archive node (here `duniter_default`)
- [ ] the host name of archive node listening on port 9944 (here `duniter-archive`)

By splitting the indexer and the archive node, you will be able to keep the archive node running while updating the indexer and possibly re-indexing.

## Running

You can start the indexer with

```sh
# start the indexer
docker compose up -d
# follow the logs
docker compose logs -f
```

In the logs, you will see the following steps:

- starting hasura engine (30 seconds)
- indexing the genesis (10 minutes)
- indexing the blockchain (2 hours)

There might be some errors passing because the indexer is under development and has some bugs. Go to https://hasura.gdev.example.org/ and enter your password to see the GraphQL API explorer and the SQL Data. 

## Nginx config

Here is an example of nginx reverse proxy config files (very basic configuration):

### Hasura on port 8080

```conf
# https redirection
server {
  listen 80 ;
  listen [::]:80 ;

  server_name hasura.gdev.example.org;

  return 301 https://$host$request_uri;
}

# https configuration
server {
  listen 443 ssl;
  listen [::]:443 ssl;

  ssl_certificate /etc/letsencrypt/live/hasura.gdev.example.org/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/hasura.gdev.example.org/privkey.pem;

  server_name hasura.gdev.example.org;

  access_log /var/log/nginx/hasura.gdev.example.org_access.log;
  error_log /var/log/nginx/hasura.gdev.example.org_error.log;

  root /var/www/hasura.gdev.example.org;

  location / {
    add_header X-Robots-Tag "noindex";
    proxy_pass http://localhost:8080;
    proxy_redirect http://localhost:8080 https://hasura.gdev.example.org;
  }
}
```

### Playground on port 3000

```conf
# https redirection
server {
  listen 80 ;
  listen [::]:80 ;

  server_name indexer.gdev.example.org;

  return 301 https://$host$request_uri;
}

# https configuration
server {
  listen 443 ssl;
  listen [::]:443 ssl;

  ssl_certificate /etc/letsencrypt/live/indexer.gdev.example.org/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/indexer.gdev.example.org/privkey.pem;

  server_name indexer.gdev.example.org;

  access_log /var/log/nginx/indexer.gdev.example.org_access.log;
  error_log /var/log/nginx/indexer.gdev.example.org_error.log;

  root /var/www/indexer.gdev.example.org;

  location / {
    add_header X-Robots-Tag "noindex";
    proxy_pass http://localhost:3000;
    proxy_redirect http://localhost:3000 https://indexer.gdev.example.org;
  }
}
```

When you are happy with your config and want to share your endpoint, publish it on the forum!