+++
title = "Indexers"
weight = 30
+++

# Indexers

Some tasks are difficult to perform by interacting directly with the blockchain. For example getting the list of transactions emitted and received by an account. The indexer is a piece of software that makes these tasks way easier for the clients. We aim to have about 3-4 indexers for each country using Ğ1 to provide enough redundancy in case some of them fail. 

- duniter forum section [https://forum.duniter.org/c/dev/indexers/66](https://forum.duniter.org/c/dev/indexers/66)

## Software

There are multiple indexer software with different strategy and different targets:

- [duniter-squid](@/wiki/duniter-v2/indexers/duniter-squid.md)
- [duniter-indexer](@/wiki/duniter-v2/indexers/duniter-indexer.md) (archived)

