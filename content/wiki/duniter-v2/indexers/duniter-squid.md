+++
title = "Duniter squid"
weight = 10
+++

# Duniter squid

Duniter squid is an indexer for Duniter v2 based on the subsquid framework.

- git repository [https://git.duniter.org/nodes/duniter-squid](https://git.duniter.org/nodes/duniter-squid)

## Install duniter-squid

### Requirements

You need an archive node running on the same network. In the following, we will assume this archive node is available in the `duniter_default` network with the url `ws://duniter-archive:9944`.

### Docker compose

The following `docker-compose.yml` allows you to start a `duniter-squid` node. Please look carefully at the `# <---` comments. To see the name of the network of your archive node, look at `docker network ls`. This example compose comes from [the one on the repo](https://git.duniter.org/nodes/duniter-squid/-/blob/main/docker-compose.yml). 

```yml
version: "3"

services:
  # postgres database
  db:
    image: postgres:15
    volumes:
      - postgres-data:/var/lib/postgresql/data
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U postgres -d ${DB_NAME}"]
      interval: 2s
      timeout: 2s
      retries: 5
    environment:
      POSTGRES_DB: ${DB_NAME}
      POSTGRES_PASSWORD: ${DB_PASSWORD}
  # hasura graphql endpoint
  hasura-squid:
    image: registry.duniter.org/nodes/duniter-squid/hasura:latest
    depends_on:
      processor:
        condition: service_healthy
    restart: always
    ports:
      - "${HASURA_LISTEN_PORT}:8080"
    environment:
      PRODUCTION: "true"
      HASURA_GRAPHQL_DATABASE_URL: postgres://postgres:${DB_PASSWORD}@db:5432/${DB_NAME}
      HASURA_GRAPHQL_ENABLE_CONSOLE: "true"
      HASURA_GRAPHQL_DEV_MODE: "false"
      HASURA_GRAPHQL_UNAUTHORIZED_ROLE: public
      HASURA_GRAPHQL_ENABLE_TELEMETRY: "false"
      HASURA_GRAPHQL_ADMIN_SECRET: ${HASURA_GRAPHQL_ADMIN_SECRET}
  # squid processor which reads duniter data and put them in the postgres database
  processor:
    image: registry.duniter.org/nodes/duniter-squid/squid:latest
    environment:
      - DB_NAME=${DB_NAME}
      - DB_PORT=5432
      - DB_HOST=db
      - DB_PASS=${DB_PASSWORD}
      - RPC_ENDPOINT=${RPC_ENDPOINT}
      - GENESIS_FILE=${GENESIS_FILE}
      - HIST_GEN_FILE=${HIST_GEN_FILE}
      - HIST_BLOCK_FILE=${HIST_BLOCK_FILE}
      - HIST_TX_FILE=${HIST_TX_FILE}
      - HIST_CERT_FILE=${HIST_CERT_FILE}
    command: ["sqd", "process:prod"]
    depends_on:
      db:
        condition: service_healthy
    healthcheck:
      test: ["CMD-SHELL", "pgrep -f 'process:prod'"]
      interval: 5s
      timeout: 2s
      retries: 3
    networks:
      - default # <--- allows the processor to connect to the postgres database
      - duniter # <--- allows the processor to connect to the duniter archive node

volumes:
  postgres-data:

networks:
  duniter:
    name: duniter-gdev-archive_default # <--- your network name might vary depending on your config / folder name
    external: true
```

And use this example environment file (from [`.env.example`](https://git.duniter.org/nodes/duniter-squid/-/blob/main/.env.example)):

```
# postgres
DB_NAME=squid
DB_PASSWORD=postgres

# processor input files (included in image)
GENESIS_FILE="./input/gdev.json"
HIST_GEN_FILE="./input/genesis.json"
HIST_BLOCK_FILE="./input/block_hist.json"
HIST_TX_FILE="./input/tx_hist.json"
HIST_CERT_FILE="./input/cert_hist.json"

# hasura
HASURA_GRAPHQL_ADMIN_SECRET=my_hasura_password
HASURA_LISTEN_PORT=8080

# Duniter RPC endpoint depending on your service name
RPC_ENDPOINT=ws://duniter-archive:9944
```

When you are ready, run the compose with:

```sh
docker compose down -v && # <--- if you want to erase previously existing data \
docker compose up -d && # <--- to start the compose in detached mode \
docker compose logs -f # <--- to see the logs and follow them
```

### Nginx config

Example nginx config:

```
server {
  listen 443 ssl;
  listen [::]:443 ssl;

  ssl_certificate /etc/letsencrypt/live/YOUR_DOMAIN/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/YOUR_DOMAIN/privkey.pem;

  server_name YOUR_DOMAIN;

  access_log /var/log/nginx/YOUR_DOMAIN_access.log;
  error_log /var/log/nginx/YOUR_DOMAIN_error.log;

  location / {
    add_header X-Robots-Tag "noindex";
    proxy_pass http://localhost:4350;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $http_connection;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto https;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header Host $host;
  }
}
```

## Use duniter-squid

Go on your `duniter-squid` url ([https://hasura.gdev.coinduf.eu/](https://hasura.gdev.coinduf.eu/) for example) and make your requests in the GraphiQL playground. Then use the requests in your client.
