+++
title = "SSH tunnel"
weight = 92
+++

# SSH tunnel

If your node is on a remote machine only listening locally, you can still emulate a local node with a ssh tunnel:

```bash
# tunnel to remote machine listening on 9944
ssh -NL 9944:localhost:9944 user@host
#         ↑ local port   ↑ remote port
```

If you configured your node to listen on a different port (example `9945` for validator)

```bash
# tunnel to remote machine listening on 9945
ssh -NL 9944:localhost:9945 user@host
```

This way you will be able to connect to your remote node using a local url `ws://localhost:9944/`.