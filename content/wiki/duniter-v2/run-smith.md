+++
title = "Run a smith node"
weight = 40

[taxonomies]
authors = ["HugoTrentesaux"]

[extra.translations]
fr = "wiki/doc-v2/installer/forgeron/"
+++

# Run a smith node

A smith node is the tool used by a smith to forge blocks. They are useful for network decentralisation and resilience. On the test networks, we aim at 20-30 smith nodes and on the production network we aim at 40-60 smith nodes. This means that we expect to build a smith web of trust with at least 80 persons (smith are not available 100% of the time).

[TOC]

## Definitions

In this context "smith node" and "validator node" are equivalent. This is only Duniter vs Substrate vocabulary (see [glossary](@/wiki/duniter-v2/glossary.md)).

## Requirements

The first step to run a smith node is to get a machine well connected to the network and with a good uptime (the time it is available). A self-hosted machine subject to power outage is discouraged if you are not experienced with this.

The power of the machine should be equal or superior to the reference machine (currently a Raspberry Pi 4, see [forum](https://forum.duniter.org/t/choix-de-la-machine-de-reference-pour-letalonnage-des-poids/9067)), but for the training network it does not matter.

You should be able to connect to your machine directly via `ssh` or a vpn.

Some practice with a [mirror node](@/wiki/duniter-v2/run-mirror.md) is appreciated, even not required.

## Docker

Here is an example `docker-compose.yml` file configured to run a validator node with a distance oracle.

```yml
# Duniter smith node
services:
  # --- smith node ---
  duniter-smith:
    image: duniter/duniter-v2s-gdev-800 # <--- here is the image
    restart: unless-stopped
    ports:
      # prometheus endpoint for monitoring
      - 127.0.0.1:9615:9615
      # private rpc endpoint (do not expose publicly)
      - 127.0.0.1:9944:9944
      # public p2p endpoint
      - 30333:30333 # <--- here is your public p2p port
    volumes:
      - duniter-data:/var/lib/duniter/
    environment:
      - DUNITER_CHAIN_NAME=gdev # <--- the network you want to connect to
      - DUNITER_NODE_NAME=YOUR_NAME-smith # <--- the name of your node on the network
      - DUNITER_VALIDATOR=true # <--- enable validator
      - DUNITER_PRUNING_PROFILE=light # <--- stays light
      - DUNITER_PUBLIC_ADDR=/dns/gdev.example.org/tcp/30333 # <--- your public p2p endpoint
      - DUNITER_LISTEN_ADDR=/ip4/0.0.0.0/tcp/30333
  # --- distance oracle ---
  distance-oracle:
    container_name: distance-oracle
    image: duniter/duniter-v2s-gdev-800 # <--- same image
    entrypoint: docker-distance-entrypoint # other entrypoint
    environment:
      ORACLE_RPC_URL: ws://duniter-smith:9944
      ORACLE_RESULT_DIR: /var/lib/duniter/chains/gdev/distance/ # should match network
      ORACLE_EXECUTION_INTERVAL: 200 # <--- should be adjusted based on network
    volumes:
      - duniter-data:/var/lib/duniter # use same volume

volumes:
  duniter-data:
```

Then follow these steps:

1. Make sure the image and chain name match the network you want to connect to {`gdev`, `gtest`, `g1`}
1. Replace `YOUR_NAME` by a name identifying you (for example your Ğ1 pseudo)
1. Copy the modified file on your server under the name `docker-compose.yml`
1. In the folder of your `docker-compose.yml` file, run:

```sh
# start the node in demon mode
docker compose up -d
# look at the logs and follow
docker compose logs -f
```

That's all. You can now [become smith](@/wiki/duniter-v2/become-smith.md) and start forging blocs. If you do so, make sure to never stop your validator node while forging blocks. About the distance oracle, you can run it [from an other compose file](@/wiki/duniter-v2/distance-oracle.md) if you prefer.


## Debian

Debian package includes both a validator node and a distance oracle.

```sh
# download with curl (temporary url, will appear in release page)
curl https://bafybeidcspicuabhgbzktzigi4jjltf6h4jvly4xf5e7tcfajddi3wh53e.ipfs.pagu.re/ -o duniter_0.8.0-1_amd64.deb
# install with apt
sudo apt install ./duniter_0.8.0-1_amd64.deb
# configure at least DUNITER_NODE_NAME in env file
sudo vim /etc/duniter/env_file
# start the service
sudo systemctl start duniter-smith.service
# follow the logs to see synchronization in progress
sudo journalctl -u duniter-smith.service -f
# enable service if you want it to start automatically on system startup
sudo systemctl enable duniter-mirror.service
```

To start forging blocks, you will have to [become smith](@/wiki/duniter-v2/become-smith.md) and declare your node session keys.

## Yunohost

For the moment, Yunohost is only easy for mirror node.

- Yunohost forum topic: <https://forum.yunohost.org/t/installation-de-duniter-v2-alpha-avec-yunohost/31143>