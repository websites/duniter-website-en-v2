+++
title = "Configure your node (Docker)"
weight = 80

[taxonomies]
authors = ["pini"]
tags= ["docker"]

[extra]
EXTERNAL_CONTENT = "https://git.duniter.org/nodes/rust/duniter-v2s/-/raw/master/docker/README.md"
auto_toc = true
+++

⚠️ the content of this file will be overwritten by EXTERNAL_CONTENT