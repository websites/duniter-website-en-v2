+++
title = "Datapods"
weight = 200
+++

# Datapods

Datapod network is an IPFS-based system for offchain storage. It is still an alpha software.

See more in [https://git.duniter.org/nodes/ipfs-datapod/](https://git.duniter.org/nodes/ipfs-datapod/).