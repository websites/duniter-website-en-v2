+++
title = "Smith"
+++

# Smith operations in polkadotjs

Here we detail the smith operations in polkadotjs. Some like `rotateKeys` require that your are connected to your smith node for example with a [ssh tunnel](@/wiki/duniter-v2/ssh-tunnel.md).

## Rotate and set session keys

How to manage session keys in polkadotjs.

{% note(type="warning", markdown=true) %}
Setting session keys manually with polkadotjs is **possible but difficult**, please use other methods instead (gcli, duniter panel) unless you know what you are doing.
{% end %}

1. be sure to have your smith key available in your browser (see [duniter-connect](@/wiki/duniter-v2/duniter-connect/_index.md) for example).
1. connect PolkadotJs to your smith node (add `?rpc=ws://localhost:9944` to your url or use the left menu).
1. go to [Dev > RPC Call > author > rotateKeys()], and copy the returned concatenated keys.
1. split the concatenated keys it in 4 equal parts corresponding to [grandpa, babe, imonline, autority discovery] protocols.
1. go to [Dev > Extrinsics > authorityMembers > setSessionKeys(keys)], and paste the 4 hex keys as argument.

## Check that session keys are present

If you are not sure that the session keys you published are present on your node, you can check manually like so.

1. Go to [developer > chain state > session > key owner], uncheck `include option` and execute the query.

<div class="center-content">
<img src="../screenshots/session-key-owner_no_include_option.png" />
</div>

2. In the response, you will have to retrieve 4 parts that correspond to your (Smith) identity key (`5Dq8xjvkmbz7q4g2LbZgyExD26VSCutfEc6n4W4AfQeVHZqz` in this example)


```
[
  ...  
  [
    [
      [
        imon
        0x9a78f3f2fa073495929493b577f86a6aefce669b2addcc9aae5be6c359015857
      ]
    ]
    5Dq8xjvkmbz7q4g2LbZgyExD26VSCutfEc6n4W4AfQeVHZqz
  ]
  ...
  [
    [
      [
        gran
        0xd26d1c17b3416cf572d94cd14eb7972cb622b3dc1a1932c2128f5a968918e04b
      ]
    ]
    5Dq8xjvkmbz7q4g2LbZgyExD26VSCutfEc6n4W4AfQeVHZqz
  ]
  ...
  [
    [
      [
        audi
        0x7c9117ece1a4eac34d7996c8c42765ba88f1167f5174da3aa2edd51d1a33bd69
      ]
    ]
    5Dq8xjvkmbz7q4g2LbZgyExD26VSCutfEc6n4W4AfQeVHZqz
  ]
  ...
  [
    [
      [
        babe
        0x68c31363f71533ab1480e8667a68fdd17be17ec1d54c1ac7c185b8ce4bd33866
      ]
    ]
    5Dq8xjvkmbz7q4g2LbZgyExD26VSCutfEc6n4W4AfQeVHZqz
  ]
```

3. Go to [developer > rpc calls > author > has session keys] and verify that your node actually has the correct session keys

<div class="center-content">
<img src="../screenshots/author-has-session-keys.png" />
</div>

The hexadecimal for the session keys is simply the concatenation of the [**gran**dpa, **babe**, **imOn**line, **au**thority**Di**scovery] keys in this order.

```
0xd26d1c17b3416cf572d94cd14eb7972cb622b3dc1a1932c2128f5a968918e04b68c31363f71533ab1480e8667a68fdd17be17ec1d54c1ac7c185b8ce4bd338669a78f3f2fa073495929493b577f86a6aefce669b2addcc9aae5be6c3590158577c9117ece1a4eac34d7996c8c42765ba88f1167f5174da3aa2edd51d1a33bd69
```

If the answer is "true", it is ok, else it is not and you should not go online.