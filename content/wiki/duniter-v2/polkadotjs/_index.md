+++
title = "PolkadotJs App"
# date = 2023-01-23
weight = 20
aliases = ["/wiki/duniter-v2/use-polkadotjs-app/"]

# [taxonomies]
# authors = ["HugoTrentesaux",]
+++

# PolkadotJs App

PolkadotJs app is an [unhosted web app](https://unhosted.org/) allowing to connect to any substrate blockchain (not just polkadot, it is wrongly named). In this tutorial, we are going to use the version provided by js.org ([https://polkadot.js.org/](https://polkadot.js.org/)), but you can use a self-hosted version if you prefer. The "read only" features are available straight ahead, but operation requiring signing documents will need [polkadotjs extension](https://addons.mozilla.org/en-US/firefox/addon/polkadot-js-extension/). You can also use the [duniter-connect](https://addons.mozilla.org/fr/firefox/addon/duniter-connect/) fork to manage keys generated with Cesium-v1.

## Duniter Portal

We have a hosted version of polkadotjs named "Duniter Portal". It is available at this address: <https://duniter-portal.axiom-team.fr/> and comes with preconfigured endpoints and some improvements based on the network.

## Connect to a node

The first thing you want to do in PolkadotJs is connect to a node. You can:

- connect to a local node running on the same machine
- connect to a remote node through a [ssh tunnel](@/wiki/duniter-v2/ssh-tunnel.md)
- connect to a remote node with its public endpoint

### Local node

If you have a node running on your local machine and listening to websockets on port 9944, you can visit:

[https://polkadot.js.org/apps/?rpc=ws://localhost:9944#/](https://polkadot.js.org/apps/?rpc=ws://localhost:9944#/)

Notice:

- `rpc` stands for "Remote Procedure Call", it's the API
- `ws` not `wss` (no TLS layer)
- port `9944` (default websocket listening port)
- `/` root path (no route)

### Private remote node <small>through ssh tunnel</small>

See [ssh tunnel](@/wiki/duniter-v2/ssh-tunnel.md)


### Public remote node <small>through URL</small>

If you want to connect to a public remote node (ex: `gdev.axiom-team.fr`) listening on port 443 (default for https) on path `/ws` you can visit:

[https://polkadot.js.org/apps/?rpc=wss://gdev.axiom-team.fr/ws#/](https://polkadot.js.org/apps/?rpc=wss://gdev.axiom-team.fr/ws#/)

The node can be changed in the left bar accessible via the first link in the menu.