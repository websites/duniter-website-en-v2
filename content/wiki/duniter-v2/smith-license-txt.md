+++
title = "Smith License (text format)"
template = "custom/license.html"
weight = 90

[extra]
license_url = "https://git.duniter.org/1000i100/g1_charter/-/raw/master/smith_charter/g1_smith_charter_fr.adoc"
+++

⚠️ The license is downloaded at build time from GitLab.