+++
title = "Ğcli"
weight = 40
+++

# Ğcli

Ğcli is a command line interface with Duniter-v2s.

- repo: [https://git.duniter.org/clients/rust/gcli-v2s/](https://git.duniter.org/clients/rust/gcli-v2s/)

Some other tools like `subkey` can be useful and are not yet integrated into gcli.

## Install

Download latest version for your platform from release page: <https://git.duniter.org/clients/rust/gcli-v2s/-/releases> (available as linux binary, debian, package, macOS package).

```sh
# check your installed version
> gcli --version
gcli 0.3.0
```


## Configure

Ğcli accepts arguments like `--url` to tell the node you want to connect to, but you might want to save it in a config file with `gcli config`:

```sh
# shows current gcli config
> gcli config show
Ğcli config
duniter endpoint ws://localhost:9944
indexer endpoint http://localhost:8080/v1/graphql
address (no address)

# save new config and shows it
> gcli --network gdev --address 5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY config save
> gcli config show
Ğcli config
duniter endpoint wss://gdev.p2p.legal:443/ws
indexer endpoint https://gdev-indexer.p2p.legal/v1/graphql
address 5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY

# you can also edit directly the config file with
> nano `gcli config where`
```

Saving an address to the config file allows you to use this address by default for gcli commands that require one.

## Secret

Ğcli accepts multiple secret formats including Cesium id/password and mnemonic.

```sh
# using Cesium login
> gcli -S cesium account balance
Cesium id: 
Cesium password: 
5E3rcvZEfHpVckY87C11LXbdonpmbjCJbvCmXCn1XKPNSKqo has 34.85 ĞD
# using mnemonic
> gcli -S substrate -s "bottom drive obey lake curtain smoke basket hold race lonely fit walk//Alice" account balance
5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY has 1721658900 ĞD
```

But that can be cumbersome to enter secret everytime. Ğcli has a vault that allows to store password-protected mnemonic (only secret format supported for now).

```sh
# see where are the secrets stored
> gcli vault where
/home/user/.local/share/gcli
# add mnemonic to the vault
> gcli vault import
Mnemonic: 
Enter password to protect the key
Password: 
Stored secret for 5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY
# list available addresses
> gcli vault list
available keys:
5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY
```

When the address defined in config file is present in the vault, you can unlock it with password.

## Use

Look at gcli help with `gcli help`.

```sh
> gcli help
A command-line interface for Duniter v2s uses

Usage: gcli [OPTIONS] <COMMAND>

Commands:
  account     Account (balance, transfer...)
  identity    Identity (get, create, confirm, revoke...)
  smith       Smith (certify, go-online, go-offline...)
  tech        Tech (list members, proposals, vote...)
  ud          Universal Dividend (claim...)
  oneshot     Oneshot account (balance, create, consume...)
  blockchain  Blockchain (current block, runtime info...)
  indexer     Indexer (check, latest block)
  config      Config (show, save...)
  vault       Key management (import, generate, list...)
  help        Print this message or the help of the given subcommand(s)

Options:
  -i, --indexer <INDEXER>              Overwrite indexer endpoint
      --no-indexer                     Do not use indexer
  -s, --secret <SECRET>                Secret key or BIP39 mnemonic (only used when secret format is compatible) (eventually followed by derivation path)
  -S, --secret-format <SECRET_FORMAT>  Secret key format (seed, substrate, cesium)
  -a, --address <ADDRESS>              Address
  -u, --url <URL>                      Overwrite duniter websocket RPC endpoint
  -n, --network <NETWORK>              Target network (local, gdev, gtest...)
      --no-wait                        prevent waiting for extrinsic completion
  -o, --output-format <OUTPUT_FORMAT>  Output format (human, json, ...) [default: human]
  -h, --help                           Print help
  -V, --version                        Print version
```

Ğcli is made to be intuitive to learn. If do not find how to do something or encounter a problem, do not hesitate to ask for support on the forum or raise an issue.