+++
title = "Smith"
+++

# Smith operations in Ğcli

This page describes smith operations in Ǧcli.

## Help

You can see the help with `gcli smith --help`:

```sh
> gcli smith --help
Smith (certify, go-online, go-offline...)

Usage: gcli smith <COMMAND>

Commands:
  go-online         go online
  go-offline        go offline
  update-keys       Rotate and set session keys
  set-session-keys  Set session keys
  show-expire       List upcoming expirations that require an action
  show-online       List online authorities
  invite            Invite identity to become smith
  accept            Accept invitation
  certify           Certify smith
  help              Print this message or the help of the given subcommand(s)
```

## Smith web of trust

These are the smith web of trust commands:

```sh
# invite a member to join the smith web of trust
gcli smith invite <pseudo>
# then, the member invited to become smith has to accept the invitation
gcli smith accept
# once accepted, people can start certifying
gcli smith cert <pseudo>
# when enough certifications are collected, the identity becomes member of the smith wor
gcli identity get -u <pseudo>
# [see smith status in output]
```
PS: You need to be member of the main Web of Trust to be smith (so if you are no longer member of the main WoT, your smith status will therefore automatically be lost).

## Smith node management

You smith node must own session keys. If you generated them from another tool, you can use `gcli smith set-session-keys <hexadecimal>` but there is the all-in-one command `update-keys` which makes things simpler.

```sh
# connect with a ssh tunnel to your smith node unsafe API (or use a vpn, or use gcli on the same machine)
ssh -NL 9944:localhost:9944 smith_node
# call update keys
gcli --url ws://localhost:9944 smith update-keys
```

The "update key" command does two things:

- call "rotate_keys" unsafe RPC methods which generates session keys on your node and returns the public part
- call "set_session_keys" extrinsic which informs the onchain storage of your smith node's session keys

⚠️ the command does not print anything on success for the moment, this should be changed in gcli

Ğcli does not yet have the capability to check if published session keys are actually present on the node, this could be added, please contribute ;)

### Smith update online state
After setting your session keys, in the case you would like to start issuing blocks, you need to go online to be part of the next set of authorities.
`go-online` will be sent to the blockchain for the next epoch. After one hour, you will enter in the online smith pool to compute blocks.
If you know your server is planned to be offline, you can do a `go-offline` to explicitly removed it from the set authorities for the next epoch.
If you don't explicitly go offline, but your server is offline, it will generate an offense and sets you offline automatically in the next epoch.
In any case you will need to go online manually when you are back.

Associated commands:
```
gcli smith go-online
gcli smith go-offline
```

## Smith management with v1 credentials on a remote server
If you are managing a v2 smith node via ssh with v1 credentials, adapt previous commands as follow:

```sh
# instead of `gcli smith accept` to accept smith invitation, use:
gcli --url ws://localhost:9944 --no-indexer --secret-format cesium smith accept

# instead of `gcli smith update-keys` use:
gcli --url ws://localhost:9944 --no-indexer --secret-format cesium smith update-keys

# instead of `gcli smith go-online` use:
gcli --url ws://localhost:9944 --no-indexer --secret-format cesium smith go-online 
```
PS: options need to be placed before the sub-command. `gcli smith go-online --url ws://localhost:9944 --no-indexer --secret-format cesium` won’t work.

PS2: `--no-indexer` is a fail-safe way to run these commands but it is not required if gcli is linked with an up-to-date indexer in its configuration.
