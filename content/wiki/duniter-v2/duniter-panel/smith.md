+++
title = "Smith"
+++

# Smith operations in Duniter Panel

Make sure you are connected with your smith identity with [Duniter Connect](@/wiki/duniter-v2/duniter-connect/_index.md).

## Smith web of trust

TODO screenshots or video tutorial

## Node management

When you are connected with your smith identity and go to your smith page, you should see the following buttons:

<div class="center-content">
<img src="../screenshots/smith-view-1.png" width=600 />
</div>

To be able to rotate keys, you must be connected directly to your smith node. Change the settings to the address of your smith node, for example `ws://localhost:9944` if you are using a [ssh tunnel](@/wiki/duniter-v2/ssh-tunnel.md) or vpn.

Once done, the **rotateKeys & send Session Keys** button should not be grayed out anymore and you should be able to simply click it.

For more details, see on the forum (french): <https://forum.duniter.org/t/session-keys-directement-dans-le-panneau-forgeron/12445>.