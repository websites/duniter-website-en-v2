+++
title = "Glossary"
date = 2023-01-22
weight = 99

[taxonomies]
authors = ["HugoTrentesaux",]

[extra.translations]
+++

# Glossary

## Duniter specific

**smith**: Someone who is given the power to *forge* blocks.

**forging blocks**: Other blockchains say *mining blocks* because this action is generally rewarded by monetary creation (metaphor with gold mining). But because Duniter unique monetary creation is *UD*, we chose an other metaphor.

## Substrate specific

**authority**: An authority is entiteled to become a validator, and then to author blocks.

**validator**: A validator node is a node configured to add blocks to the blockchain. By extension, validators are people holding the keys of these nodes.
