+++
title = "Become smith"
date = 2023-01-23
weight = 30

[taxonomies]
authors = ["HugoTrentesaux",]
+++

# Become smith

In Duniter v2 you can only *forge* blocks if your identity is *smith* (see [glossary](@/wiki/duniter-v2/glossary.md)). This page guides you to get the *smith* status. If you previously followed the [run a smith node](@/wiki/duniter-v2/run-smith.md) tutorial, you will be able to enable forging blocks. The steps to complete are the following one:

1. become member of the main web of trust, ([read how](@/wiki/g1v2/become-member.md))
1. read, understand, accept and start to follow the [smith licence](@/wiki/duniter-v2/smith-license-txt.md)
1. run a smith node ([read how](@/wiki/duniter-v2/run-smith.md))
1. be invited to join smith with `smithMembers.InviteSmith()`
1. accept invitation with `smithMembers.AcceptInvitation()`
1. get enough certifications from other smith with `smithMembers.CertifySmith()` until promoted
1. set session keys with `authorityMembers.set_session_keys()` extrinsic
1. start forging blocks with `authorityMembers.goOnline()` 

The complete process is detailed below, but high level tools like [Ğcli](@/wiki/duniter-v2/gcli/_index.md), [Tikka](@/wiki/duniter-v2/tikka/_index.md) or [Duniter Panel](@/wiki/duniter-v2/duniter-panel/_index.md) can be helpful.

[TOC]

### Become member of the main web of trust

Duniter is based on human trust without implying peculiary interest. Therefore, there is no need to own a large quantity of currency to take part to the blockchain authoring, only to be part of the main web of trust. This implies having read and accepted the Ğ1 licence and getting certified by other wot members (see [g1 documentation](@/wiki/g1/_index.md)). Having a unique digital identity entails you can only have one smith identity.

### Read and understand the smith licence

The [smith licence](@/wiki/duniter-v2/smith-license-txt.md) defines the rules to certify a new Duniter smith. These rules aim to garantee a good operation of Duniter blockchain by making sure that the new smith knows the rules applying to this role and that other smiths trust his/her ability to apply them.

### Accept invitation to the smith group

Before getting smith certifications, you have to be invited and accept the invitation (see [runtime calls reference](/wiki/duniter-v2/runtime-calls/#accept-invitation)). After that, you will have to get certs and go online within a certain delay.

### Get enough certifications from other smith

The minimum number of received certifications to become smith depends on the runtime. Currently it is:

- **2** for ĞDev
- **3** for ĞTest and Ğ1 runtimes

A smith is only able to become an authority (see [glossary](@/wiki/duniter-v2/glossary.md)) once he/she get enough certifications before the membership request expiration.

### Set session keys

Once you are member of the smith wot, you are allowed to declare session keys with `authorityMembers.set_session_keys()`. The argument of `set_session_keys` is the four pubkeys of {**babe**, **grand**pa, **imon**line, **au**thority **di**scovery} protocols. These keys must be present on your smith node and should be generated locally to avoid leak.

You can find detailed documentation on how to do it with the following tools:

- [Ğcli](@/wiki/duniter-v2/gcli/smith.md), command line toole
- [Duniter Panel](@/wiki/duniter-v2/duniter-panel/smith.md), dedicated minimalist app
- [Polkadotjs](@/wiki/duniter-v2/polkadotjs/smith.md), generic tool from substrate ecosystem (difficult)

### Start forging blocks

You can enter the authority set by calling `authorityMembers.goOnline()`. Before that, you can double-check a few things:
- your validator node is well synchronized to the network
- your validator node actually has the session keys you declared

Otherwise you will be unable to add blocks and will generate BABE offences.

---

### Stay smith

Once you are smith, you must:

- not stay offline more than *48 sessions* (2 days)
- not lose your main wot membership (renew membership)
- not trigger offences (TODO document)

Otherwise, you will be excluded from the smith and will have to start over.