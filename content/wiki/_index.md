+++
title = "Wiki"
# date = 2023-01-22
weight = 2
sort_by = "weight"
description = "Documentation about all Duniter ecosystem"

# [taxonomies]
# authors = ["HugoTrentesaux",]
# tags = ["wiki", "blockchain",]

[extra.translations]
fr = "wiki/"
+++

# Wiki

The goal of this wiki is to guide the reader in the Duniter software ecosystem. You will find keys for a global understanding and software user documentation (the developer documentation can be read directly in the different projects repository).

#### Duniter software

Duniter software is the core of Duniter ecosystem. It is a custom blockchain implementation intended to run both on server and desktop.

[➡️ read more about Duniter](@/wiki/doc/_index.md)

#### Libre Money

*Libre* money is the monetary theory behind Duniter. It defines the concept of Universal Dividend (UD).

[➡️ read more about libre money](@/wiki/libre-money/_index.md)

#### Ğ1 currency

Ğ1 currency is the production network currently running on Duniter v1. 

[➡️ read more about Ğ1 currency](@/wiki/g1/_index.md)

#### Web of trust

The web of trust is a decentralised identity system implemented in Duniter.

[➡️ read more about web of trust](@/wiki/web-of-trust/_index.md)

{% note(type="warning", markdown="true", display="block") %}
#### Duniter software (v2)

Duniter is being currently rewritten in the substrate framework. 

[➡️ read more about Duniter v2](@/wiki/duniter-v2/_index.md)

#### Ğ1 currency (v2)

Ğ1v2 is the continuity of Ğ1 network, based on Duniter v2 software.

[➡️ read more about Ǧ1v2](@/wiki/g1v2/_index.md)
{% end %}