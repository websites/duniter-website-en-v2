+++
title = "Web of trust"
weight = 60
sort_by = "weight"

[extra.translations]
fr = "wiki/toile-de-confiance/"
+++

# Web of trust

A web of trust ([wikipedia](https://en.wikipedia.org/wiki/Web_of_trust)) is a network of key signatures. It has been used for a while with PGP to establish a decentralized trust network for encrypted communications and proof of authenticity. Duniter's web of trust aims to add unicity of digital identity (one person = one key) and to ensure frequent renewal of the signatures. This has several applications from Duniter-specific Universal Dividend and more general voting systems or proof of identity.

## Vocabulary

Here is some Duniter specific vocabulary and the equivalent in graph theory ([wikipedia](https://en.wikipedia.org/wiki/Graph_theory)).

| word | explanation | graph equivalent | description |
| - | - | - | - |
| identity | living person uniquely associated with a cryptographic key | vertex, node ([wikipedia](https://en.wikipedia.org/wiki/Vertex_(graph_theory))) | node in the network |
| membership | fact for an identity for being part of the web of trust | | |
| certification | dated and cryptographically signed document assessing IRL trust | edge, link | edge in the network |
| identity indegree | number of certifications received by an identity | node indegree ([wikipedia](https://en.wikipedia.org/wiki/Directed_graph#Indegree_and_outdegree)) | number of links pointing towards a node |
| identity outdegree | number of certifications emitted by an identity | node outdegree ([wikipedia](https://en.wikipedia.org/wiki/Directed_graph#Indegree_and_outdegree)) | number of links coming from a node |
| | | (in/out)neighborhood of a node ([wikipedia](https://en.wikipedia.org/wiki/Neighbourhood_(graph_theory))) | subgraph made of adjacent nodes (using only in/out links) 

## Rules

We can divide the rules of Duniter web of trust into static and temporal rules describing the state of a snapshot of the graph and the evolution of it along time.

{% note(type="info", display="block")%}
This simplified version of the rules aim to give a global view. Additionnal detail can be found in the references.
{% end %}

### Static rules

The static rules describe the graph properties we can observe on a snapshot of the web of trust.

- minimum indegree is **5**
- maximum outdegree is **100**

The number of identities member of the web of trust is written **N**.

### Dynamic rules

The dynamic rules describe the evolution of the web of trust along time.

- and identity must renew its membership every **1 year** and is removed from the wot otherwise
- a certification must be renewed every **2 years** and is removed from the wot otherwise
- a certification can only be submitted more than **5 days** after the previous one (new or renewal)

### Entry rule

In addition to the previous rules, an identity **I** must comply with the distance rule to enter the web of trust. The distance rule is defined as follows:

- an identity with indegree and outdegree greater than `N^(1/5)` is called "referent"
- the 6-inneighborhood of **I** must contain at least **80%** of the total number of referents

The distance rules applies on identity entry and membership renewal.

## History

Duniter Ğ1 web of trust started on 2017-03-08 with 59 identities and 551 certifications at the block 0 of Duniter Ğ1 blockchain. As of 2023-09-25 it counts 8449 member identities and 98756 active certifications. This graph shows the evolution of the member count along time.

![plot members along time](/img/members.png)
> evolution of the member count along time  
> made with [DataJune](https://git.42l.fr/HugoTrentesaux/DataJune.jl)

We can see that part of the identities expire after not being renewed during the 1 year period. This ensures all the member identities are actively using their digital identity. It is possible to display the evolution of this web of trust using a force-directed layout.


<a href="https://youtu.be/Hj3GpaEYLwA" target="_blank" class="center-content"><img src="/blog/wot_2022-11-22.png" alt="web of trust snapshot" style="max-width: 600px;"/></a>
> snapshot of the web of trust (2022-11-22)  
> animation is visible on [https://youtu.be/Hj3GpaEYLwA](https://youtu.be/Hj3GpaEYLwA)


## References

- [Introducing the web of trust](@/wiki/web-of-trust/introducing-web-of-trust.md)
- [Deep dive into the web of trust](@/wiki/web-of-trust/deep-dive-wot.md)
- [RFC of the protocol](https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md?ref_type=heads)
- [Reference implementation Duniter (v1)](https://git.duniter.org/nodes/typescript/duniter/)
- [Reference implementation Duniter (v2)](https://git.duniter.org/nodes/rust/duniter-v2s/)