+++
title = "License (text format)"
template = "custom/license.html"

[extra]
license_url = "https://git.duniter.org/documents/g1_monetary_license/-/raw/master/g1_monetary_license_en.rst"
+++

⚠️ The license is downloaded at build time from GitLab.