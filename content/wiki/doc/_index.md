+++
title = "Duniter v1"
# date = 2017-04-08
weight = 10
sort_by = "weight"

# [taxonomies]
# authors = ["cgeek",]

[extra.translations]
fr = "wiki/doc/"
+++

# Duniter Software

These pages document the use of Duniter software. To understand what this software do, you can read first:

- [Introduction](@/blog/2015-08-11-introduction.md)
- [Theoretical](@/blog/2015-08-11-theoretical.md)

## Installation

* [Install your Duniter node](@/wiki/doc/install/index.md)
* [Update your Duniter node](@/wiki/doc/update.md)
* [Update a Duniter node hosted on YunoHost](https://forum.duniter.org/t/full-https-support-for-duniter-package-for-yunohost/1892/18)

## For Developers

* [Duniter CLI](@/wiki/doc/commands.md)
* Creating a release [for Windows](@/wiki/contribute/archive/create_win_release.md) and [for ARM](@/wiki/contribute/archive/create_arm_release.md)

## Concepts

* [Architecture](@/wiki/contribute/archive/architecture.md)
* [The Proof-of-Work in Duniter](@/wiki/contribute/archive/duniter-proof-of-work.md)

