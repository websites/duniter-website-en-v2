+++
title = "Update a Duniter node"
date = 2017-06-19
weight = 4

[taxonomies]
authors = ["cgeek",]

[extra.translations]
fr = "wiki/doc/mettre-a-jour/"
+++

# Update a Duniter node

Updating its Duniter node just consists in:

* [reinstall its Duniter node](@/wiki/doc/install/index.md)
* restart Duniter to apply the modifications

