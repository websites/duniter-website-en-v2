+++
title = "matograine"
description = "développeur de ViĞnette, Ğ1Cotis et Ğ1dons, contributeur à Silkaj, et au whitepaper"

[extra]
avatar = "matograine.png"
forum_duniter = "matograine"
forum_ml = "matograine"

[taxonomies]
authors = ["matograine",]
+++

Développeur de ViĞnette (JS), Ğ1Cotis et Ğ1dons (python). Contributeur à Silkaj. Compilateur du brouillon de Whitepaper.