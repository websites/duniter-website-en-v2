+++
title = "immae"
description = "adminsys/devops for Duniter infra"

[extra]
full_name = "Immae"
avatar = "immae.jpg"
website = "https://www.immae.eu/"
forum_duniter = "immae"
g1_pubkey = "8YJ5aBV8NVnhawt8RTsENhxfb27PQd84NtgJK2sYuqDp"
email = "contact@mail.immae.eu"
gitduniter = "immae"
g1_map = false # (des)active le lien vers la carte ML

[taxonomies]
authors = ["immae",]
+++

Je suis un développeur et sysadmin, je travaille avec et pour des outils libres et je m’intéresse un peu aux cryptomonnaies.