+++
title = "kimamila"
description = "Cesium and Ğchange developer"

[extra]
full_name = "Benoit Lavenier"
avatar = "Kimamila.png"
forum_duniter = "kimamila"
forum_ml = "kimamila"
g1_pubkey = "38MEAZN68Pz1DTvT3tqgxx4yQP6snJCQhPqEFxbDk4aE"
gitduniter = "blavenie"

[taxonomies]
authors = ["kimamila",]
+++

Kimamila is the author of Cesium v1 and Cesium v2. Also of Gchange platform and Cesium+ pods.