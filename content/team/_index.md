+++
title = "Team"
# date = 2020-07-29
weight = 5
template = "custom/team.html"
page_template = "authors/page.html"

[extra]
# contrôle la liste des contributeurs (nom de fichier)
authors = [
    "cgeek",
    "kimamila",
    "Moul",
    "1000i100",
    "vit",
    "Paidge",
    "gerard94",
    "tuxmain",
    "poka",
    "HugoTrentesaux",
    "manutopik",
    "kapis",
    "vjrj",
    "txels",
    "bgallois",
    "immae",
    "pini",
    "elois",
    ]
+++

We list here the people who have made a technical contribution to the project. To appear here, please complete [the dedicated page on the forum](https://forum.duniter.org/t/lequipe-qui-travaille-sur-quoi/9076), or make a PR on the [site repository](https://git.duniter.org/websites/duniter-website-en-v2).