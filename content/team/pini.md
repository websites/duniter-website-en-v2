+++
title = "pini"
description = "docker expert, simplifies life of all"

[extra]
full_name = "Gilles Filippini"
avatar = "pini.png"
website = "https://pini.fr/"
forum_duniter = "pini"
forum_ml = "pini"
g1_pubkey = "8Dy5S4KwQ5SWXucEoRw41aRzBxxzXQk3EAYy9CFxSWA2"
gitduniter = "pini-gh"
matrix = "@pini:pini.fr"

[taxonomies]
authors = ["pini",]
+++

Contributions à de nombreuses images Docker.