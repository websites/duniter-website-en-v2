+++
title = "vincentux"
description = "Vincentux"

[extra]
avatar = "vincentux.jpeg"
forum_duniter = "vincentux"
forum_ml = "vincentux"
g1_pubkey = "5dzkzedBWdeqTFCaD7AkKPMPusfRUL1XyFNJWWGYQ9f1"
website = "https://contact.vincentux.fr/"

[taxonomies]
authors = ["vincentux",]
+++