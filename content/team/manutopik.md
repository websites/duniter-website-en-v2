+++
title = "manutopik"
description = "developer of monnaie-libre.fr website and its map"

[extra]
full_name = "Emmanuel Salomon"
avatar = "ManUtopiK_square2.jpg"
forum_duniter = "manutopik"
forum_ml = "manutopik"
g1_pubkey = "2JggyyUn2puL5PG6jsMYFC2y9KwjjMmy2adnx3c5fUf8"
g1_map = true
gitduniter = "manutopik"


[taxonomies]
authors = ["manutopik",]
+++

**N'hésitez pas à faire un don en Ğ1 😍**

#### Contributions

- Développement et rédaction du site [monnaie-libre.fr](https://monnaie-libre.fr) (Dépôt [du site](https://git.duniter.org/websites/monnaie-libre-fr), [de l'api](https://git.duniter.org/websites/api-monnaie-libre))
- Développement de la [carte.monnaie-libre.fr](https://carte.monnaie-libre.fr) ([Dépôt](https://git.duniter.org/websites/carte-g1))
- [Doc silkaj](https://git.duniter.org/websites/doc_silkaj)
- [WotWizard-UI](https://git.duniter.org/clients/wotwizard-ui)
- [g1lib](https://git.duniter.org/libs/g1lib.js)

#### En cours

- [Extension web g1Compagnon](https://git.duniter.org/clients/g1companion)
- Interface web pour g1Billet
