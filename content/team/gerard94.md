+++
title = "gerard94"
description = "WotWizard developer"

[extra]
full_name = "Gérard Meunier"
avatar = "Gerard.jpeg"
forum_duniter = "gerard94"
forum_ml = "gerard94"
website = "http://gemeu.free.fr/"
g1_pubkey = "CRBxCJrTA6tmHsgt9cQh9SHcCc8w8q95YTp38CPHx2Uk"
gitduniter = "gerard94"

[taxonomies]
authors = ["gerard94",]
+++

Enseignant de physique en lycée à la retraite. Programmeur par passion (Component Pascal sous BlackBox). Je m’intéresse particulièrement à la logique mathématique. Constructeur de la première imprimante 3D libre (Reprap) en France en 2008/2009.