+++
title = "axiom-team"
description = "association pour le développement technique de la Ğ1"

[extra]
full_name = "Axiom Team"
avatar = "axiom-team.png"
website = "https://axiom-team.fr/"
g1_pubkey = "8CWuf4f1jYoVzHh4DEFpCyzZYC1pgz4t2wU8F2zKCthh"
email = "contact@axiom-team.fr"
+++

Axiom Team organization supports technical development of Ğ1 Libre Currency through funding of Duniter software ecosystem.

Axiom Team sur le web :

- website <https://axiom-team.fr/>
- HelloAsso <https://www.helloasso.com/associations/axiom-team/>
- group on the french forum <https://forum.monnaie-libre.fr/g/axiom-team>

Members :

- [poka](@/team/poka.md)
- [Hugo](@/team/HugoTrentesaux.md)
- [tuxmain](@/team/tuxmain.md)
- [1000i100](@/team/1000i100.md)
- [elois](@/team/elois.md)
- [Manutopik](@/team/manutopik.md)
- [Matograine](@/team/matograine.md)
- Pi
- Attilax
- Paulart
- Scanlegentil
- Syoul