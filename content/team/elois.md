+++
title = "Elois"
description = "initial developer of Duniter v2"

[extra]
avatar = "elois.jpg"
website = "https://librelois.fr/"
gitduniter = "librelois"
+++

Elois