+++
title = "cgeek"
description = "author of Duniter v1 and contributor to Duniter v2"

[extra]
full_name = "Cédric Moreau"
avatar = "cgeek.png"
website = "https://blog.cgeek.fr/"
gitduniter = "c-geek"
forum_duniter = "cgeek"
forum_ml = "cgeek"
+++

Cédric Moreau is the main developer of Duniter v1 and contributor of Duniter v2.