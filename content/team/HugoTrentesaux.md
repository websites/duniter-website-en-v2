+++
title = "HugoTrentesaux"
description = "maintainer of duniter.org website and contributor on Duniter v2"

[extra]
full_name = "Hugo Trentesaux"
avatar = "HugoTrentesaux.png"
website = "https://trentesaux.fr/"
forum_duniter = "HugoTrentesaux"
forum_ml = "Hugo-Trentesaux"
g1_pubkey = "55oM6F9ZE2MGi642GGjhCzHhdDdWwU6KchTjPzW7g3bp"
g1_map = true
phone = "+33 6 49 88 18 21"
email = "hugo@trentesaux.fr"
xmpp = "h30x@militant.es"
gitduniter = "HugoTrentesaux"

[extra.translations]
fr = ""
+++

I discovered Ğ1 in 2017 thanks to free and decentralized social networks. I contributed to the web of trust growth around Paris and did some minor contributions on softwares like Dunitrust, Duniterpy, Ǧecko, ForceAtlas2-rs... I made the migration of Duniter website on Zola SSG to be able to maintain it in the long run. I like to have a deep understanding of technical aspects and document them in an easy to understand manner.