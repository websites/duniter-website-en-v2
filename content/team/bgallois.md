+++
title = "bgallois"
description = "developer on Duniter v2"

[extra]
full_name = "Benjamin Gallois"
avatar = "bgallois.png"
website = "https://gallois.cc/"
forum_duniter = "bgallois"
gitduniter = "bgallois"
+++

Developer on Duniter v2. Paid by [Axiom-Team](https://axiom-team.fr/) organization with [ADEME](https://www.ademe.fr/en/) funding.