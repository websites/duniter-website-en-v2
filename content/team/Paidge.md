+++
title = "Paidge"
description = "developer of WotwizardUI"

[extra]
full_name = "Pierre-Jean Chancellier"
avatar = "Paidge.png"
forum_duniter = "paidge"
forum_ml = "paidge"
gitduniter = "paidge"

[taxonomies]
authors = ["Paidge",]
+++

Paidge is the developer of [Wotmap](https://wotmap.duniter.org/) and WotwizardUI.