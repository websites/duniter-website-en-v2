+++
title = "How to get Ğ1"
date = 2017-05-31
weight = 2

[taxonomies]
authors = ["cgeek", "inso",]
category = ["FAQ",]
+++

# How to get Ğ1

## Get it

Whether you are an individual, an association, a company or a robot, you can obtain currency units of Ğ1. As of March 31, 2017, there are ** 15,370.00 Ğ1 ** outstanding.

To do this, it is simple: create a wallet Ğ1 then find someone ready to make an exchange. Yes but an exchange of what? Of units Ğ1 for the one who owns it, against a good or service for the one who wants it.

The major marketplace is [ĞChange](https://www.gchange.fr).

## Produce

But if you are an individual not yet producing units Ğ1, know that *you could produce them yourself*.

> This is indeed one of the fundamental characteristics of Ğ1: every human being can, subject to acceptance by his peers, produce his legitimate share of money.

We advise you first to try the *test network* and its currency ĞTest. There are no tutorials in english, so feel free to produce it !

Once sufficiently seasoned with ĞTest, you can then try to move to Ğ1. You can start by *bringing you closer to people already members of Ğ1 and knowing you*, their certifications being necessary to become a member. Most of the members are french and belgian right now, so this could require a bit of effort if you are not living in France or Belgium right now.

> ** /!\ Caution: Adhering to the currency Ğ1 implies the acceptance of its license, a copy of which you will find below. **

[Read the Ğ1 license (text format)](@/wiki/g1/license-txt.md)

