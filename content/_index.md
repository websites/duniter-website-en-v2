+++
title = "Home"
template = "custom/1-home.html"
sort_by = "weight"
description = "Duniter is a blockchain software powering the Ğ1 libre currency."

# zola ignores section's taxonomies, so they are directly in template `cutsom/1-home.html`
# [taxonomies]
# tags = ["Duniter", "Libre Money", "Ğ1", "currency", "cryptocurrency", "blockchain"]
+++
