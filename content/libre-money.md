+++
title = "Libre Money"
template = "custom/2-monnaie-libre.html"
weight = 1
description = "A libre currency is a currency that respects the principles of spatial and temporal symmetry. Free money creation does not introduce inequalities between individuals."

[taxonomies]
tags = ["libre money", "libre currency", "TRM"]
+++
