# Fork of duniter.fr website

This is a fork of duniter.fr website with english translations imported from duniter.org.

TODO before publication:
- [x] translate Ǧ1 and Softwares pages
- [ ] translate wiki pages and FAQ
- [x] link between articles and their translation counterpart
- [ ] translate meta (this readme for example)
- [ ] add contribution guide
- [ ] document future maintainance (cherry-pick theme for example)
- [x] add CI
- [x] organize taxonomies (categories, tags)
- [x] link to license
- [x] track dead internal links
- [x] fix broken image links
- [x] translate description html meta
- [x] replace `/!\` by warning notes
- [x] translate 404


# Duniter website

This site is built with Zola and serves as technical support to present and use the Duniter software. It also introduces the ground concepts of libre money and the workings of the ğ1 currency.

## Usage

To modify the site locally, start by cloning the repo:

```
git clone https://git.duniter.org/websites/duniter_website_fr_v2.git
```

Most of the components are included, but certain ones like Forkawesome (for icon fonts) are available as git submodules. You may include them with:

```
git submodule init
git submodule update
```

After installing [Zola](https://www.getzola.org/documentation/getting-started/installation/), run this in the working copy's root folder:

```
zola serve
```

and click on the link [http://127.0.0.1:1111](http://127.0.0.1:1111) to check out the site locally (it auto-reloads upon changes).

To update the docs automatically from the git content, run the purpose-built script (be aware it's a prototype and may fail) :

```
pip install toml
./scripts/get_external_content.py
```

## Vue d'ensemble

Most of the site's content lives in the folder `content` as Markdown (rich text) files, with metadata headings. Static files (images, documents) live for the most part under `static`, a large subset under `PELICAN`, a folder resulting from the migration of the initial site. The theme consists of templated html files that live under `templates`. Finally, some stylesheets are written in sass (a superset of css) in the `sass` folder, while others are written in plain css and live alongside the static files.

## Contribute

In order to simply contribute, there is no need to be familiar with `git`, `zola`, `html`, `css`, `javascript`, `markdown`. It will suffice to follow the [contribution guide](./doc/README.md).
